let pintado = false; // En el juego , si se selecciona una imagen esta var se pone en true
let letraSelec = null;

var arreglo = [];
var arreglo2 = ["pato", "pelo", "fila", "pala", "pera", "foco", "pies", "foca", "foto"]

async function cargarCartas(){
    arreglo["pato"] = false;
    arreglo["pelo"] = false;
    arreglo["fila"] = false;
    arreglo["pala"] = false;
    arreglo["pera"] = false;
    arreglo["foco"] = false;
    arreglo["pies"] = false;
    arreglo["foca"] = false;
    arreglo["foto"] = false;
    for(i=0;i<10;i++){
        const selector = document.getElementById(arreglo2[i]);
        selector.classList.add('magictime', 'tinUpIn');
        document.getElementById(arreglo2[i]).style.display = 'inline-block';
        playSound('../sonidos/lentos/'+arreglo2[i]+'.wav')
        await new Promise(resolve => setTimeout(resolve, 5000));
    }
}

function comenzar(){
    document.getElementById("cartelComenzar").style.display = 'none';
    document.getElementById("juego").style.display = 'block';
    cargarCartas()
};

function enmarcarVarias(event) {
    var carta = event.target;
    if (arreglo[carta.id] == false) {
        carta.className += " cambiarBorde";
        arreglo[carta.id] = true;
    } else {
        document.getElementById(carta.id).classList.remove("cambiarBorde");
        arreglo[carta.id] = false;
    }
}

function comprobar(s) {
    if (checkTableP('p') & checkTableF('f')) {
        confirmar(s);
    } else {
        alerta();
    }
}

function checkTableP(letra) {
    var tabla = $('#' + letra);
    var items = tabla.children('tbody').children('tr').find('img');
    var cont = 0;
    var padre;
    var hijo;
    for (var i = 0; i < items.length; i++) {
        if (items[i].dataset.letra != letra){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }
        if (items[i].dataset.letra == letra){
            cont++;
        }
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
                helper: 'clone'
                
    });

    $('.box-image').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);

        }
    });

   return (cont==5);
}

function checkTableF(letra) {
    var tabla = $('#' + letra);
    var items = tabla.children('tbody').children('tr').find('img');
    var cont = 0;
    var padre;
    var hijo;
    for (var i = 0; i < items.length; i++) {
        if (items[i].dataset.letra != letra){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }
        if (items[i].dataset.letra == letra){
            cont++;
        }
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
                helper: 'clone'
                
    });

    $('.box-image').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);

        }
    });

   return (cont==4);
}

function clickNumero(numero){
    if(numero == 4){
        document.getElementById('form13').style.display = 'block';
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function escribirSeguido(p){
    if(p=="letraUno"){
        document.getElementById("letraDos").focus();
    } else if(p=="letraDos"){
        document.getElementById("letraTres").focus();
    } else if(p=="letraTres"){
        document.getElementById("letraCuatro").focus();
    } else if(p=="letraCuatro"){
        document.getElementById("letraCinco").focus();
    }
}

function corregirPala(p){
  let letraUno = document.forms["form13"]["letraUno"].value;
  let letraDos = document.forms["form13"]["letraDos"].value;
  let letraTres = document.forms["form13"]["letraTres"].value;
  let letraCuatro = document.forms["form13"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'l' && letraCuatro.toLowerCase() == 'a'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirPelo(p){
  let letraUno = document.forms["form13"]["letraUno"].value;
  let letraDos = document.forms["form13"]["letraDos"].value;
  let letraTres = document.forms["form13"]["letraTres"].value;
  let letraCuatro = document.forms["form13"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 'l' && letraCuatro.toLowerCase() == 'o'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirPato(p){
  let letraUno = document.forms["form13"]["letraUno"].value;
  let letraDos = document.forms["form13"]["letraDos"].value;
  let letraTres = document.forms["form13"]["letraTres"].value;
  let letraCuatro = document.forms["form13"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 't' && letraCuatro.toLowerCase() == 'o'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirPera(p){
  let letraUno = document.forms["form13"]["letraUno"].value;
  let letraDos = document.forms["form13"]["letraDos"].value;
  let letraTres = document.forms["form13"]["letraTres"].value;
  let letraCuatro = document.forms["form13"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 'r' && letraCuatro.toLowerCase() == 'a'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirPies(p){
  let letraUno = document.forms["form13"]["letraUno"].value;
  let letraDos = document.forms["form13"]["letraDos"].value;
  let letraTres = document.forms["form13"]["letraTres"].value;
  let letraCuatro = document.forms["form13"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'e' && letraCuatro.toLowerCase() == 's'){
      confirmarFinal(p)
  } else {
      AlertError();
  }
}

const quitarAnimacion = () => {
    let element = document.getElementById("ayudabtn");
    element.classList.remove("pulse");
    let ocultarMano = document.getElementById("manoayuda")
    ocultarMano.classList.add("imgOcultar")
}

const ocultarElement = id => {
  let elem = document.getElementById(id)
  elem.classList.add("imgOcultar")
}

const quitarAnim = id => {
  let elem = document.getElementById(id)
  elem.classList.remove("pulse")
}

const mostrarElement = id => {
  let elem = document.getElementById(id)
  elem.classList.remove("imgOcultar")
}

const animar = (id) => {
  let elem = document.getElementById(id)
  elem.classList.add("pulse")
}

const mostrarImgPrincipal = (url,titulo) => {
 
  let tabla = document.getElementById("tabla1-1")
  tabla.classList.remove("imgOcultar")
  tabla.classList.add("tabla1-1")

  let elementImg = document.getElementById("imgj1");
  elementImg.classList.add("imgJuegosG")
  console.log('elemimg',elementImg)
  let elementTitulo = document.getElementById("tituloj1");
  elementImg.src=url;
  elementTitulo.innerHTML= titulo
}

/*Selecciona con color una imagen elegida, y comprueba que 
no haya otra seleccionada, en ese caso, la despinta, y pinta la nueva */
function enmarcar(event) {

  animar('j1-btncheck')
  mostrarElement('manoayuda3')

  let element = document.getElementById("manoayuda2")
  element.classList.add("imgOcultar")
 
  let selec = event.target;
  if (pintado == false) {
    selec.className += "cambiarBorde";
    pintado = true;
    letraSelec = selec.id;
   
  } else {
    $(".cambiarBorde").removeClass("cambiarBorde");
    selec.className += " cambiarBorde";
    letraSelec = selec.id;
   
  }
}
var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");

/*Cartelito*/

function confirmar(s) {
  sndOK.play();
  alertify.alert(
    "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    function () {
        alertify.success("CARGANDO...");
        setTimeout(function () {
          window.location.href = "../html/" + s + ".html"; //Pasa al siguiente juego
        }, 1300);
    }
  );
  return false;
}

function alerta() {
  //un alert
  sndNO.play();
  alertify.alert(
    "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
    function () {
      //aqui introducimos lo que haremos tras cerrar la alerta.
    }
  );
}

//JuegoExtra
const checkGameExtra = (s) => {
  let a = checkTableGameExtra('tabla1',['o','l','a'])
  let b = checkTableGameExtra('tabla2',['o','s','o'])
  let c = checkTableGameExtra('tabla3',['u','n','o'])
  if(a && b && c){
    confirmarFinal(s)
  }else{
    alerta()
  }
  
}


const checkTableGameExtra = (tablaId, letras) => {

    let tabla = $('#' + tablaId);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;
    let padre;
    let hijo;
   

    for (let i = 0; i < items.length; i++) {

        if (items[i].dataset.letra == letras[i]){
          cont++;
        }
        if (items[i].dataset.letra != letras[i]){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }
       
        
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
                helper: 'clone'
            });

            $('.box-image-rayitas').droppable({
                accept: '.item',
                hoverClass: 'hovering',
                drop: function(ev, ui) {
                    ui.draggable.detach();
                    $(this).append(ui.draggable);
                }
    });
   return (cont==3);
}

