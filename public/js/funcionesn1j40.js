var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var pintado = false;
let arreglo = ["xapa", "xinela", "harina", "helado"];
var indexGeneral = 0;
var palabraActual = '';
var primeraVez = true;

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
}

function confirmar2() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/nivel1.html";
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function faltanimg() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h1><b> &iexcl; ALGO NO ESTA BIEN ! <br> &iexcl; INTENTALO DE NUEVO ! </b></h1>", function() {
        //aqui introducimos lo que haremos tras cerrar la alerta.
    });
}

function checkTableGameExtraJ6(){
    let tabla = $('#tabla-'+palabraActual);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;


    for (let i = 0; i < items.length; i++) {

        if (items[i].dataset.letra == palabraActual.charAt(i)){
            cont++;
        }
        if (items[i].dataset.letra != palabraActual.charAt(i)){
            alerta();
            return false;
        }

    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
        helper: 'clone'
    });

    $('.box-image-rayitas').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);
        }
    });
    
    document.getElementById(palabraActual).style.display = 'none';
    listadoPalabras(palabraActual, 'none', 'img-');
    listadoPalabras(palabraActual, 'none', '');
    return true;
}

function checkGame(){
    let a = checkTableGameExtraJ6()
    if(a){
        if(arreglo.length==0){
            confirmar2()
        } else {
            confirmar();
            document.getElementById("divSobre").style.display = "inline-block";
            document.getElementById("divTable").style.display = "none";
            document.getElementById("divCajitas").style.display = "none";
        }
    }
}

async function generarJuego(palabra){
    document.getElementById(palabra).style.display = 'inline-block';
    listadoPalabras(palabra, 'inline-block', '')
}

function listadoPalabras(palabra, directiva, string){
    document.getElementById('tabla-'+palabra).style.display = directiva;
    switch (palabra){
        case 'helado':
            document.getElementById(string+'helado-h').style.display = directiva;
            document.getElementById(string+'helado-e').style.display = directiva;
            document.getElementById(string+'helado-l').style.display = directiva;
            document.getElementById(string+'helado-a').style.display = directiva;
            document.getElementById(string+'helado-d').style.display = directiva;
            document.getElementById(string+'helado-o').style.display = directiva;
            break;

        case 'harina':
            document.getElementById(string+'harina-h').style.display = directiva;
            document.getElementById(string+'harina-a').style.display = directiva;
            document.getElementById(string+'harina-r').style.display = directiva;
            document.getElementById(string+'harina-i').style.display = directiva;
            document.getElementById(string+'harina-n').style.display = directiva;
            document.getElementById(string+'harina-a-2').style.display = directiva;
            break;

        case 'xapa':
            document.getElementById(string+'xapa-ch').style.display = directiva;
            document.getElementById(string+'xapa-a').style.display = directiva;
            document.getElementById(string+'xapa-p').style.display = directiva;
            document.getElementById(string+'xapa-a-2').style.display = directiva;
            break;

        case 'xinela':
            document.getElementById(string+'xinela-ch').style.display = directiva;
            document.getElementById(string+'xinela-i').style.display = directiva;
            document.getElementById(string+'xinela-n').style.display = directiva;
            document.getElementById(string+'xinela-e').style.display = directiva;
            document.getElementById(string+'xinela-l').style.display = directiva;
            document.getElementById(string+'xinela-a').style.display = directiva;
            break;
    }
}

function funcionGeneral(){
    document.getElementById("divSobre").style.display = "none";
    document.getElementById("divTable").style.display = "inline-block";
    document.getElementById("divCajitas").style.display = "flex";
    if(arreglo.length>=1){
        var numeroRandom = Math.floor(Math.random() * arreglo.length);
        if(primeraVez){
            palabraAnterior = arreglo[numeroRandom];
            primeraVezVez = false;
        } else {
            palabraAnterior = palabraActual;
        }
        palabraActual = arreglo[numeroRandom];
        arreglo.splice(numeroRandom,1);
        generarJuego(palabraActual);
    }  else {
        alert('Terminó el juego')
    }
}