var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var pintado = false

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/n1j17Beso.html"; //Pasa al siguiente juego
            }, 1300);
        }
    );
    return false;
}

function confirmar2(p) {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/"+p+".html"; //Pasa al siguiente juego
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function estaBien() {
    sndOK.play();
    alertify.alert( "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>");
}



//Le devuelvo la propiedad arrastrable a cada imagen
$('.item').draggable({
    helper: 'clone'
});

function clickNumero(numero){
    if(numero == 4){
        document.getElementById('formFoca').style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function clickNumeroCinco(numero){
    if(numero == 5){
        document.getElementById('formFoca').style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function escribirSeguido(p){
    if(p=="letraUno"){
        document.getElementById("letraDos").focus();
    } else if(p=="letraDos"){
        document.getElementById("letraTres").focus();
    } else if(p=="letraTres"){
        document.getElementById("letraCuatro").focus();
    } else if(p=="letraCuatro"){
        document.getElementById("letraCinco").focus();
    }
}

function escribirSeguidoLetraDoble(p){
    if(document.getElementById(p).value.length > 1){
        if(p=="letraUno"){
            document.getElementById("letraDos").focus();
        } else if(p=="letraDos"){
            document.getElementById("letraTres").focus();
        } else if(p=="letraTres"){
            document.getElementById("letraCuatro").focus();
        } else if(p=="letraCuatro"){
            document.getElementById("letraCinco").focus();
        }
    }
    
}

function corregirBeso(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 'b' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 's' && letraCuatro.toLowerCase() == 'o'){
        confirmar2(p);
    } else {
        AlertError();
    }
}

function corregirBurro(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 'b' && letraDos.toLowerCase() == 'u' && letraTres.toLowerCase() == 'rr' && letraCuatro.toLowerCase() == 'o'){
        confirmar2(p);
    } else {
        AlertError();
    }
}

function corregirBarco(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    let letraCinco = document.forms["formFoca"]["letraCinco"].value;
    if(letraUno.toLowerCase() == 'b' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'r' && letraCuatro.toLowerCase() == 'c' && letraCinco.toLowerCase() == 'o'){
        confirmar2(p);
    } else {
        AlertError();
    }
}

function checkPalabra(palabra){
    if(palabra == 'bota' || palabra == 'burro' || palabra == 'beso'){
        if(palabra == 'bota'){
            estaBien();
            document.getElementById('bote').style.display = 'none'
            document.getElementById('bota').style.display = 'none'
            document.getElementById('beto').style.display = 'none'
            document.getElementById('imagenBota').style.display = 'none'
            document.getElementById('barro').style.display = 'inline-block'
            document.getElementById('buzo').style.display = 'inline-block'
            document.getElementById('burro').style.display = 'inline-block'
            document.getElementById('imagenBurro').style.display = 'inline-block'
        }
        if(palabra == 'burro'){
            estaBien();
            document.getElementById('barro').style.display = 'none'
            document.getElementById('buzo').style.display = 'none'
            document.getElementById('burro').style.display = 'none'
            document.getElementById('imagenBurro').style.display = 'none'
            document.getElementById('peso').style.display = 'inline-block'
            document.getElementById('beso').style.display = 'inline-block'
            document.getElementById('bello').style.display = 'inline-block'
            document.getElementById('imagenBeso').style.display = 'inline-block'
        }
        if(palabra == 'beso'){
            confirmarFinal();
        }
    } else {
        alerta()
    }
}