var ganador = new Audio("../sonidos/sonidoganador.ogg");

function playsonido() {
    ganador.play();
}

function ingresar(val) {
    var sndSelec = new Audio("sonidos/selec.wav"); //menu ppal
    var sndSelec1 = new Audio("../sonidos/selec.wav"); //menu niveles
    sndSelec.play();
    sndSelec1.play();
    setTimeout(function() {
        window.location.href = val;
    }, 300);
}

const reproducir = s =>  {
    var sonido = new Audio("../sonidos/lentos/" + s);
    sonido.play();
  }

function reproducirSonido(s) {
    var sonido = new Audio("../sonidos/" + s);
    sonido.play();
}

function reproducirSonido2(s) {
    var sonido = new Audio("../sonidos/lentos/" + s);
    sonido.play();
}

function confirmSalida() {
    var sndSelec = new Audio("../sonidos/selec.wav");
    sndSelec.play();
    alertify.success("<img src='../assets/flechaizquierda.png'> SALIENDO... ");
    setTimeout(function() {
        window.location.href = '../html/nivel1.html';
    }, 1300);
}

function confirmarFinal() {
    sndOK.play();
    alertify.alert(
      "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
      function () {
        alertify.error("ELEGISTE '" + alertify.labels.cancel + "'");
        confirmSalida();
      }
    );
    return false
  }

function ayuda(){
     setTimeout(function() {
        alertify.alert("<img src='../img/ayudaw.png'> ");}, 1300);
      }

async function reload(){
    alertify.success("CARGANDO...");
    await new Promise(resolve => setTimeout(resolve, 2000));
    location.reload();
}
  
  

  

