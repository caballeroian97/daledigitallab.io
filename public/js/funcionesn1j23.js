var dama= false;
var duro = false;
var duna= false;
var bollo = false;
var banana = false;
var baño = false;

var arreglo = ["dama","bollo", "banana", "duro", "duna", "baño"];

function comenzar(){
    document.getElementById("cartelComenzar").style.display = 'none';
    document.getElementById("juego").style.display = 'block';

    reproducirLetras();
};

async function reproducirLetras(){
  for(i=0;i<6;i++){
    const selector = document.getElementById(arreglo[i]);
    playSound('../sonidos/'+arreglo[i]+'.wav')
    await new Promise(resolve => setTimeout(resolve, 3000));
  }
}

function clickNumero(numero){
  if(numero == 3){
      document.getElementById('letraD').style.display = 'none'
      document.getElementById('3d').style.display = 'none'
      
      document.getElementById('letraB').style.display = 'block';
      document.getElementById('3b').style.display = 'inline-block';
  } else {
      AlertError();
  }
}

function escribirSeguidoDama(p){
  if(p=="letraUnoDama"){
      document.getElementById("letraDosDama").focus();
  } else if(p=="letraDosDama"){
      document.getElementById("letraTresDama").focus();
  } else if(p=="letraTresDama"){
      document.getElementById("letraCuatroDama").focus();
  }
}

function escribirSeguidoDuro(p){
  if(p=="letraUnoDuro"){
      document.getElementById("letraDosDuro").focus();
  } else if(p=="letraDosDuro"){
      document.getElementById("letraTresDuro").focus();
  } else if(p=="letraTresDuro"){
      document.getElementById("letraCuatroDuro").focus();
  }
}

function escribirSeguidoDuna(p){
  if(p=="letraUnoDuna"){
      document.getElementById("letraDosDuna").focus();
  } else if(p=="letraDosDuna"){
      document.getElementById("letraTresDuna").focus();
  } else if(p=="letraTresDuna"){
      document.getElementById("letraCuatroDuna").focus();
  }
}

function escribirSeguidoBollo(p){
  if(p=="letraUnoBollo"){
      document.getElementById("letraDosBollo").focus();
  } else if(p=="letraDosBollo"){
      document.getElementById("letraTresBollo").focus();
  } else if(p=="letraTresBollo" && document.getElementById(p).value.length > 1){
      document.getElementById("letraCuatroBollo").focus();
  }
}

function escribirSeguidoBanana(p){
  if(p=="letraUnoBanana"){
      document.getElementById("letraDosBanana").focus();
  } else if(p=="letraDosBanana"){
      document.getElementById("letraTresBanana").focus();
  } else if(p=="letraTresBanana"){
      document.getElementById("letraCuatroBanana").focus();
  } else if(p=="letraCuatroBanana"){
      document.getElementById("letraCincoBanana").focus();
  } else if(p=="letraCincoBanana"){
    document.getElementById("letraSeisBanana").focus();
  }
}

function escribirSeguidoBaño(p){
  if(p=="letraUnoBaño"){
      document.getElementById("letraDosBaño").focus();
  } else if(p=="letraDosBaño"){
      document.getElementById("letraTresBaño").focus();
  } else if(p=="letraTresBaño"){
      document.getElementById("letraCuatroBaño").focus();
  }
}

function corregirDama(){
  let letraUno = document.forms["form22dama"]["letraUnoDama"].value;
  let letraDos = document.forms["form22dama"]["letraDosDama"].value;
  let letraTres = document.forms["form22dama"]["letraTresDama"].value;
  let letraCuatro = document.forms["form22dama"]["letraCuatroDama"].value;
  if(letraUno.toLowerCase() == 'd' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'm' && letraCuatro.toLowerCase() == 'a'){
    dama = true;
  } 
}

function corregirDuro(){
  let letraUno = document.forms["form22duro"]["letraUnoDuro"].value;
  let letraDos = document.forms["form22duro"]["letraDosDuro"].value;
  let letraTres = document.forms["form22duro"]["letraTresDuro"].value;
  let letraCuatro = document.forms["form22duro"]["letraCuatroDuro"].value;
  if(letraUno.toLowerCase() == 'd' && letraDos.toLowerCase() == 'u' && letraTres.toLowerCase() == 'r' && letraCuatro.toLowerCase() == 'o'){
    duro = true;
  } 
}

function corregirDuna(){
  let letraUno = document.forms["form22duna"]["letraUnoDuna"].value;
  let letraDos = document.forms["form22duna"]["letraDosDuna"].value;
  let letraTres = document.forms["form22duna"]["letraTresDuna"].value;
  let letraCuatro = document.forms["form22duna"]["letraCuatroDuna"].value;
  if(letraUno.toLowerCase() == 'd' && letraDos.toLowerCase() == 'u' && letraTres.toLowerCase() == 'n' && letraCuatro.toLowerCase() == 'a'){
    duna = true;
  } 
}

function corregirBollo(){
  let letraUno = document.forms["form22bollo"]["letraUnoBollo"].value;
  let letraDos = document.forms["form22bollo"]["letraDosBollo"].value;
  let letraTres = document.forms["form22bollo"]["letraTresBollo"].value;
  let letraCuatro = document.forms["form22bollo"]["letraCuatroBollo"].value;
  if(letraUno.toLowerCase() == 'b' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 'll' && letraCuatro.toLowerCase() == 'o'){
    bollo = true;
  }
}

function corregirBanana(){
  let letraUno = document.forms["form22banana"]["letraUnoBanana"].value;
  let letraDos = document.forms["form22banana"]["letraDosBanana"].value;
  let letraTres = document.forms["form22banana"]["letraTresBanana"].value;
  let letraCuatro = document.forms["form22banana"]["letraCuatroBanana"].value;
  let letraCinco = document.forms["form22banana"]["letraCincoBanana"].value;
  let letraSeis = document.forms["form22banana"]["letraSeisBanana"].value;
  if(letraUno.toLowerCase() == 'b' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'n' && letraCuatro.toLowerCase() == 'a' && letraCinco.toLowerCase() == 'n' && letraSeis.toLowerCase() == 'a'){
    banana = true;
  } 
}

function corregirBaño(){
  let letraUno = document.forms["form22baño"]["letraUnoBaño"].value;
  let letraDos = document.forms["form22baño"]["letraDosBaño"].value;
  let letraTres = document.forms["form22baño"]["letraTresBaño"].value;
  let letraCuatro = document.forms["form22baño"]["letraCuatroBaño"].value;
  if(letraUno.toLowerCase() == 'b' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'ñ' && letraCuatro.toLowerCase() == 'o'){
    baño = true;
  }
}

function corregir(p){
  corregirDama();
  corregirDuro();
  corregirDuna();
  corregirBollo();
  corregirBanana();
  corregirBaño();

  if(dama && duro && duna && bollo && banana && baño){
    confirmarFinal();
  } else {
    alerta();
  }
}

function alerta() {
  sndNO.play();
  alertify.alert(
      "<img src='../assets/triste.png'> <h3> &iexcl; FALTAN CARTAS ! </b></h3>",
      function () {
          //aqui introducimos lo que haremos tras cerrar la alerta.
      }
  );
}

/*Cartelito*/

function confirmar(s) {
  sndOK.play();
  alertify.alert(
    "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
    function () {
        window.location.href = "../html/" + s + ".html";
    }
  );
  return false;
}

function alerta() {
  //un alert
  sndNO.play();
  alertify.alert(
    "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
    function () {
      //aqui introducimos lo que haremos tras cerrar la alerta.
    }
  );
}

