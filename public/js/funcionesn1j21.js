var cartaActual;
var cartas =  ['pala', 'pila', 'piso', 'tijera', 'tubo', 'vaca'];
var max = 6;
var numeroActual;



function clickNumero(numero){
    if(cartaActual.length == numero){
        document.getElementById('form'+cartaActual).style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
        document.getElementById(cartaActual+"-letraUno").focus();
    } else {
        AlertError();
    }
}

function sacarCarta() {
    document.getElementById('divCaja').style.display = 'none'
    cartaActual = cartas[getRandomArbitrary()];
    max --;
    document.getElementById(cartaActual).style.display = 'inline-block';
    document.getElementById('tablero').style.display = 'inline-block';
}

function getRandomArbitrary() {
    numeroActual = Math.floor(Math.random() * max);
    return numeroActual;
}

function confirmarPalabra(){
    sndOK.play();
    if(max == 0){
        termino();
    } else {
        alertify.alert("<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>");
        cartas.splice(numeroActual, 1);
        document.getElementById(cartaActual).style.display = 'none';
        document.getElementById('tablero').style.display = 'none';
        document.getElementById('divCaja').style.display = 'block'
    }
    return false;
}

function escribirSeguido(p){
    if(p==cartaActual+"-letraUno"){
        document.getElementById(cartaActual+"-letraDos").focus();
    } else if(p==cartaActual+"-letraDos"){
        document.getElementById(cartaActual+"-letraTres").focus();
    } else if(p==cartaActual+"-letraTres"){
        document.getElementById(cartaActual+"-letraCuatro").focus();
    } else if(p==cartaActual+"-letraCuatro" && cartaActual.length > 4){
        document.getElementById(cartaActual+"-letraCinco").focus();
    } else if(p==cartaActual+"-letraCinco"){
        document.getElementById(cartaActual+"-letraSeis").focus();
    }
}

function corregirVaca(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno.toLowerCase() == 'v' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'c' && letraCuatro.toLowerCase() == 'a'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirTubo(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno.toLowerCase() == 't' && letraDos.toLowerCase() == 'u' && letraTres.toLowerCase() == 'b' && letraCuatro.toLowerCase() == 'o'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirTijera(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    let letraCinco = document.forms["form"+cartaActual][cartaActual+"-letraCinco"].value;
    let letraSeis = document.forms["form"+cartaActual][cartaActual+"-letraSeis"].value;
    if(letraUno.toLowerCase() == 't' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'j' && letraCuatro.toLowerCase() == 'e' && letraCinco.toLowerCase() == 'r' && letraSeis.toLowerCase() == 'a'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirPiso(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 's' && letraCuatro.toLowerCase() == 'o'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirPala(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'l' && letraCuatro.toLowerCase() == 'a'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirPila(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'l' && letraCuatro.toLowerCase() == 'a'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function termino() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/nivel1.html"; //Pasa al siguiente juego
            }, 1300);
        }
    );
    return false;
}
