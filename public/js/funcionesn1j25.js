var cartas = ["medalla", "musica", "navaja", "mula", "noche", "nuez"]
var cartaActual;
var numero = 1;
var left = 6


window.onload = function() {
    cartaActual = cartas[getRandomArbitrary()];
    numero++
    showForm();
}

function showForm(){
    document.getElementById(cartaActual).style.display = 'block'
    document.getElementById(cartaActual+"-letraUno").focus();
}

function getRandomArbitrary() {
    numeroActual = Math.floor(Math.random() * left);
    return numeroActual;
}

function continuar(){
    document.getElementById(cartaActual).setAttribute('style', 'display:none !important');
    cartas.splice(cartas.indexOf(cartaActual), 1)
    left--
    if(numero <= 6){
        cartaActual = cartas[getRandomArbitrary()];
        numero++
        showForm();
    } else {
        confirmarFinal();
    }
}

function escribirSeguido(p){
    if(p==cartaActual+"-letraUno"){
        document.getElementById(cartaActual+"-letraDos").focus();
    } else if(p==cartaActual+"-letraDos"){
        document.getElementById(cartaActual+"-letraTres").focus();
    } else if(p==cartaActual+"-letraTres"){
        document.getElementById(cartaActual+"-letraCuatro").focus();
    } else if(p==cartaActual+"-letraCuatro"){
        document.getElementById(cartaActual+"-letraCinco").focus();
    } else if(p==cartaActual+"-letraCinco"){
        document.getElementById(cartaActual+"-letraSeis").focus();
    }
}

function escribirSeguidoLetraDoble(p){
    if(document.getElementById(p).value.length > 1){
        if(p==cartaActual+"-letraUno"){
            document.getElementById(cartaActual+"-letraDos").focus();
        } else if(p==cartaActual+"-letraDos"){
            document.getElementById(cartaActual+"-letraTres").focus();
        } else if(p==cartaActual+"-letraTres"){
            document.getElementById(cartaActual+"-letraCuatro").focus();
        } else if(p==cartaActual+"-letraCuatro"){
            document.getElementById(cartaActual+"-letraCinco").focus();
        } else if(p==cartaActual+"-letraCinco"){
            document.getElementById(cartaActual+"-letraSeis").focus();
        }
    }

}

function correcto() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
    return false;
}

function corregirMedalla(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    let letraCinco = document.forms["form"+cartaActual][cartaActual+"-letraCinco"].value;
    let letraSeis = document.forms["form"+cartaActual][cartaActual+"-letraSeis"].value;
    if(letraUno == 'm' && letraDos == 'e' && letraTres == 'd' && letraCuatro == 'a' && letraCinco == 'll' && letraSeis == 'a'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirMusica(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    let letraCinco = document.forms["form"+cartaActual][cartaActual+"-letraCinco"].value;
    let letraSeis = document.forms["form"+cartaActual][cartaActual+"-letraSeis"].value;
    if(letraUno == 'm' && letraDos == 'u' && letraTres == 's' && letraCuatro == 'i' && letraCinco == 'c' && letraSeis == 'a'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirNavaja(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    let letraCinco = document.forms["form"+cartaActual][cartaActual+"-letraCinco"].value;
    let letraSeis = document.forms["form"+cartaActual][cartaActual+"-letraSeis"].value;
    if(letraUno == 'n' && letraDos == 'a' && letraTres == 'v' && letraCuatro == 'a' && letraCinco == 'j' && letraSeis == 'a'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirMula(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno == 'm' && letraDos == 'u' && letraTres == 'l' && letraCuatro == 'a'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirNoche(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno == 'n' && letraDos == 'o' && letraTres == 'ch' && letraCuatro == 'e'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirNuez(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno == 'n' && letraDos == 'u' && letraTres == 'e' && letraCuatro == 'z'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}