let pintado = false; // En el juego , si se selecciona una imagen esta var se pone en true
let letraSelec = null;

var arreglo = [];
var arreglo2 = ["tijera", "pirata", "tortuga", "tambor", "pan", "patin", "pollo", "piso", "taza"]

async function cargarCartas(){
    arreglo["tijera"] = false;
    arreglo["pirata"] = false;
    arreglo["tortuga"] = false;
    arreglo["tambor"] = false;
    arreglo["pan"] = false;
    arreglo["patin"] = false;
    arreglo["pollo"] = false;
    arreglo["piso"] = false;
    arreglo["taza"] = false;
    for(i=0;i<10;i++){
        const selector = document.getElementById(arreglo2[i]);
        selector.classList.add('magictime', 'tinUpIn');
        document.getElementById(arreglo2[i]).style.display = 'inline-block';
        playSound('../sonidos/lentos/'+arreglo2[i]+'.wav')
        await new Promise(resolve => setTimeout(resolve, 5000));
    }
}

function comenzar(){
    document.getElementById("cartelComenzar").style.display = 'none';
    document.getElementById("juego").style.display = 'block';
    cargarCartas()
};

function enmarcarVarias(event) {
    var carta = event.target;
    if (arreglo[carta.id] == false) {
        carta.className += " cambiarBorde";
        arreglo[carta.id] = true;
    } else {
        document.getElementById(carta.id).classList.remove("cambiarBorde");
        arreglo[carta.id] = false;
    }
}

function comprobar(s) {
    if (checkTableP('p') & checkTableT('t')) {
        confirmar(s);
    } else {
        alerta();
    }
}

function checkTableP(letra) {
    var tabla = $('#' + letra);
    var items = tabla.children('tbody').children('tr').find('img');
    var cont = 0;
    var padre;
    var hijo;
    for (var i = 0; i < items.length; i++) {
        if (items[i].dataset.letra != letra){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }
        if (items[i].dataset.letra == letra){
            cont++;
        }
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
                helper: 'clone'
                
    });

    $('.box-image').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);

        }
    });

   return (cont==5);
}

function checkTableT(letra) {
    var tabla = $('#' + letra);
    var items = tabla.children('tbody').children('tr').find('img');
    var cont = 0;
    var padre;
    var hijo;
    for (var i = 0; i < items.length; i++) {
        if (items[i].dataset.letra != letra){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }
        if (items[i].dataset.letra == letra){
            cont++;
        }
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
                helper: 'clone'
                
    });

    $('.box-image').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);

        }
    });

   return (cont==4);
}

function clickNumero(numero){
    if(numero == 4){
        document.getElementById('form15').style.display = 'block';
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function clickNumero6(numero){
  if(numero == 6){
      document.getElementById('form15').style.display = 'block'
      document.getElementById('tablero').style.display = 'none';
  } else {
      AlertError();
  }
}

function escribirSeguido(p){
  if(p=="letraUno"){
      document.getElementById("letraDos").focus();
  } else if(p=="letraDos"){
      document.getElementById("letraTres").focus();
  } else if(p=="letraTres"){
      document.getElementById("letraCuatro").focus();
  } else if(p=="letraCuatro"){
      document.getElementById("letraCinco").focus();
  } else if(p=="letraCinco"){
    document.getElementById("letraSeis").focus();
  }
}

function corregirPiso(p){
  let letraUno = document.forms["form15"]["letraUno"].value;
  let letraDos = document.forms["form15"]["letraDos"].value;
  let letraTres = document.forms["form15"]["letraTres"].value;
  let letraCuatro = document.forms["form15"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 's' && letraCuatro.toLowerCase() == 'o'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirTaza(p){
  let letraUno = document.forms["form15"]["letraUno"].value;
  let letraDos = document.forms["form15"]["letraDos"].value;
  let letraTres = document.forms["form15"]["letraTres"].value;
  let letraCuatro = document.forms["form15"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 't' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'z' && letraCuatro.toLowerCase() == 'a'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirTijera(p){
  let letraUno = document.forms["form15"]["letraUno"].value;
  let letraDos = document.forms["form15"]["letraDos"].value;
  let letraTres = document.forms["form15"]["letraTres"].value;
  let letraCuatro = document.forms["form15"]["letraCuatro"].value;
  let letraCinco = document.forms["form15"]["letraCinco"].value;
  let letraSeis = document.forms["form15"]["letraSeis"].value;
  if(letraUno.toLowerCase() == 't' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'j' && letraCuatro.toLowerCase() == 'e' && letraCinco.toLowerCase() == 'r' && letraSeis.toLowerCase() == 'a'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirPirata(p){
  let letraUno = document.forms["form15"]["letraUno"].value;
  let letraDos = document.forms["form15"]["letraDos"].value;
  let letraTres = document.forms["form15"]["letraTres"].value;
  let letraCuatro = document.forms["form15"]["letraCuatro"].value;
  let letraCinco = document.forms["form15"]["letraCinco"].value;
  let letraSeis = document.forms["form15"]["letraSeis"].value;
  if(letraUno.toLowerCase() == 'p' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'r' && letraCuatro.toLowerCase() == 'a' && letraCinco.toLowerCase() == 't' && letraSeis.toLowerCase() == 'a'){
     confirmar(p);
  } else {
      AlertError();
  }
}

function checkPalabra(palabra){
  if(palabra == 'pato' || palabra == 'tele' || palabra == 'perro'){
      if(palabra == 'pato'){
          estaBien();
          document.getElementById('paso').style.display = 'none'
          document.getElementById('palo').style.display = 'none'
          document.getElementById('pato').style.display = 'none'
          document.getElementById('imagenPato').style.display = 'none'
          document.getElementById('tela').style.display = 'inline-block'
          document.getElementById('tilo').style.display = 'inline-block'
          document.getElementById('tele').style.display = 'inline-block'
          document.getElementById('imagenTele').style.display = 'inline-block'
      }
      if(palabra == 'tele'){
          estaBien();
          document.getElementById('tela').style.display = 'none'
          document.getElementById('tilo').style.display = 'none'
          document.getElementById('tele').style.display = 'none'
          document.getElementById('imagenTele').style.display = 'none'
          document.getElementById('pelo').style.display = 'inline-block'
          document.getElementById('peso').style.display = 'inline-block'
          document.getElementById('perro').style.display = 'inline-block'
          document.getElementById('imagenPerro').style.display = 'inline-block'
      }
      if(palabra == 'perro'){
          confirmarFinal();
      }
  } else {
    alerta()
  }
}

function estaBien() {
  sndOK.play();
  alertify.alert( "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>");
}

function alerta() {
  sndNO.play();
  alertify.alert(
      "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
      function () {
          //aqui introducimos lo que haremos tras cerrar la alerta.
      }
  );
}

const quitarAnimacion = () => {
    let element = document.getElementById("ayudabtn");
    element.classList.remove("pulse");
    let ocultarMano = document.getElementById("manoayuda")
    ocultarMano.classList.add("imgOcultar")
}

const ocultarElement = id => {
  let elem = document.getElementById(id)
  elem.classList.add("imgOcultar")
}

const quitarAnim = id => {
  let elem = document.getElementById(id)
  elem.classList.remove("pulse")
}

const mostrarElement = id => {
  let elem = document.getElementById(id)
  elem.classList.remove("imgOcultar")
}

const animar = (id) => {
  let elem = document.getElementById(id)
  elem.classList.add("pulse")
}

const mostrarImgPrincipal = (url,titulo) => {
 
  let tabla = document.getElementById("tabla1-1")
  tabla.classList.remove("imgOcultar")
  tabla.classList.add("tabla1-1")

  let elementImg = document.getElementById("imgj1");
  elementImg.classList.add("imgJuegosG")
  console.log('elemimg',elementImg)
  let elementTitulo = document.getElementById("tituloj1");
  elementImg.src=url;
  elementTitulo.innerHTML= titulo
}

/*Selecciona con color una imagen elegida, y comprueba que 
no haya otra seleccionada, en ese caso, la despinta, y pinta la nueva */
function enmarcar(event) {

  animar('j1-btncheck')
  mostrarElement('manoayuda3')

  let element = document.getElementById("manoayuda2")
  element.classList.add("imgOcultar")
 
  let selec = event.target;
  if (pintado == false) {
    selec.className += "cambiarBorde";
    pintado = true;
    letraSelec = selec.id;
   
  } else {
    $(".cambiarBorde").removeClass("cambiarBorde");
    selec.className += " cambiarBorde";
    letraSelec = selec.id;
   
  }
}
var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");

/*Cartelito*/

function confirmar(s) {
  sndOK.play();
  alertify.alert(
    "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    function () {
        alertify.success("CARGANDO...");
        setTimeout(function () {
          window.location.href = "../html/" + s + ".html"; //Pasa al siguiente juego
        }, 1300);
    }
  );
  return false;
}

function alerta() {
  //un alert
  sndNO.play();
  alertify.alert(
    "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
    function () {
      //aqui introducimos lo que haremos tras cerrar la alerta.
    }
  );
}

//JuegoExtra
const checkGameExtra = (s) => {
  let a = checkTableGameExtra('tabla1',['o','l','a'])
  let b = checkTableGameExtra('tabla2',['o','s','o'])
  let c = checkTableGameExtra('tabla3',['u','n','o'])
  if(a && b && c){
    confirmarFinal(s)
  }else{
    alerta()
  }
  
}


const checkTableGameExtra = (tablaId, letras) => {

    let tabla = $('#' + tablaId);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;
    let padre;
    let hijo;
   

    for (let i = 0; i < items.length; i++) {

        if (items[i].dataset.letra == letras[i]){
          cont++;
        }
        if (items[i].dataset.letra != letras[i]){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }
       
        
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
                helper: 'clone'
            });

            $('.box-image-rayitas').droppable({
                accept: '.item',
                hoverClass: 'hovering',
                drop: function(ev, ui) {
                    ui.draggable.detach();
                    $(this).append(ui.draggable);
                }
    });
   return (cont==3);
}

