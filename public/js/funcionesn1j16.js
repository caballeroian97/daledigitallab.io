var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var pintado = false

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/n1j16Dado.html"; //Pasa al siguiente juego
            }, 1300);
        }
    );
    return false;
}

function confirmar2() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/n1j18y.html"; //Pasa al siguiente juego
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function estaBien() {
    sndOK.play();
    alertify.alert( "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>");
}



//Le devuelvo la propiedad arrastrable a cada imagen
$('.item').draggable({
    helper: 'clone'
});

function clickNumero(numero){
    if(numero == 4){
        document.getElementById('formFoca').style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function clickNumeroTres(numero){
    if(numero == 3){
        document.getElementById('formFoca').style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function escribirSeguido(p){
    if(p=="letraUno"){
        document.getElementById("letraDos").focus();
    } else if(p=="letraDos"){
        document.getElementById("letraTres").focus();
    } else if(p=="letraTres"){
        document.getElementById("letraCuatro").focus();
    } else if(p=="letraCuatro"){
        document.getElementById("letraCinco").focus();
    }
}

function corregirDado(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 'd' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'd' && letraCuatro.toLowerCase() == 'o'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function corregirDedo(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 'd' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 'd' && letraCuatro.toLowerCase() == 'o'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function corregirDos(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    if(letraUno.toLowerCase() == 'd' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 's'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function checkPalabra(palabra){
    if(palabra == 'dedo' || palabra == 'dino' || palabra == 'luna'){
        if(palabra == 'dedo'){
            estaBien();
            document.getElementById('dado').style.display = 'none'
            document.getElementById('duda').style.display = 'none'
            document.getElementById('dedo').style.display = 'none'
            document.getElementById('imagenDedo').style.display = 'none'
            document.getElementById('dino').style.display = 'inline-block'
            document.getElementById('dona').style.display = 'inline-block'
            document.getElementById('duna').style.display = 'inline-block'
            document.getElementById('imagenDino').style.display = 'inline-block'
        }
        if(palabra == 'dino'){
            estaBien();
            document.getElementById('dino').style.display = 'none'
            document.getElementById('dona').style.display = 'none'
            document.getElementById('duna').style.display = 'none'
            document.getElementById('imagenDino').style.display = 'none'
            document.getElementById('luna').style.display = 'inline-block'
            document.getElementById('lupa').style.display = 'inline-block'
            document.getElementById('lucha').style.display = 'inline-block'
            document.getElementById('imagenLuna').style.display = 'inline-block'
        }
        if(palabra == 'luna'){
            confirmarFinal();
        }
    } else {
        alerta()
    }

    
}

function chequearTablas(){
    let tablaImagenes = $('#imagenes');
    let itemsImagenes = tablaImagenes.children('img');

    for (let i = 0; i < itemsImagenes.length; i++) {

        if (itemsImagenes[i].classList[0] != 'itemsD'){
            alertaReinicio();
        }
        
    }

    let tablaTacho = $('#tachito');
    let itemsTacho = tablaTacho.children('img');

    for (let i = 0; i < itemsTacho.length; i++) {

        if (itemsTacho[i].classList[0] != 'otherItems'){
            alertaReinicio();
        }
        
    }

    confirmar();
}

function alertaReinicio() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            reload();
        }
    );
}
