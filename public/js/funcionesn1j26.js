var cartas = ["cafe", "carreta", "barrilete", "gusano", "gallina", "gota"]
var cartaActual;
var numero = 1;
var left = 6


function sacarCarta() {
    document.getElementById('divCaja').style.display = 'none'
    cartaActual = cartas[getRandomArbitrary()];
    numero++
    showForm();
}

function showForm(){
    document.getElementById(cartaActual).style.display = 'block'
    document.getElementById(cartaActual+"-letraUno").focus();
}

function getRandomArbitrary() {
    numeroActual = Math.floor(Math.random() * left);
    return numeroActual;
}

function continuar(){
    document.getElementById(cartaActual).setAttribute('style', 'display:none !important');
    document.getElementById('divCaja').style.display = 'block'
    cartas.splice(cartas.indexOf(cartaActual), 1)
    left--
    if(numero == 6){
        confirmarFinal()
    }
}

function escribirSeguido(p){
    if(p==cartaActual+"-letraUno"){
        document.getElementById(cartaActual+"-letraDos").focus();
    } else if(p==cartaActual+"-letraDos"){
        document.getElementById(cartaActual+"-letraTres").focus();
    } else if(p==cartaActual+"-letraTres"){
        document.getElementById(cartaActual+"-letraCuatro").focus();
    } else if(p==cartaActual+"-letraCuatro"){
        document.getElementById(cartaActual+"-letraCinco").focus();
    } else if(p==cartaActual+"-letraCinco"){
        document.getElementById(cartaActual+"-letraSeis").focus();
    } else if(p==cartaActual+"-letraSeis"){
        document.getElementById(cartaActual+"-letraSiete").focus();
    } else if(p==cartaActual+"-letraSiete"){
        document.getElementById(cartaActual+"-letraOcho").focus();
    }
}

function escribirSeguidoLetraDoble(p){
    if(document.getElementById(p).value.length > 1){
        if(p==cartaActual+"-letraUno"){
            document.getElementById(cartaActual+"-letraDos").focus();
        } else if(p==cartaActual+"-letraDos"){
            document.getElementById(cartaActual+"-letraTres").focus();
        } else if(p==cartaActual+"-letraTres"){
            document.getElementById(cartaActual+"-letraCuatro").focus();
        } else if(p==cartaActual+"-letraCuatro"){
            document.getElementById(cartaActual+"-letraCinco").focus();
        } else if(p==cartaActual+"-letraCinco"){
            document.getElementById(cartaActual+"-letraSeis").focus();
        }
    }

}

function correcto() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
    return false;
}

function corregirCafe(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno == 'c' && letraDos == 'a' && letraTres == 'f' && letraCuatro == 'e'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirCarreta(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    let letraCinco = document.forms["form"+cartaActual][cartaActual+"-letraCinco"].value;
    let letraSeis = document.forms["form"+cartaActual][cartaActual+"-letraSeis"].value;
    if(letraUno == 'c' && letraDos == 'a' && letraTres == 'rr' && letraCuatro == 'e' && letraCinco == 't' && letraSeis == 'a'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirBarrilete(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    let letraCinco = document.forms["form"+cartaActual][cartaActual+"-letraCinco"].value;
    let letraSeis = document.forms["form"+cartaActual][cartaActual+"-letraSeis"].value;
    let letraSiete = document.forms["form"+cartaActual][cartaActual+"-letraSiete"].value;
    let letraOcho = document.forms["form"+cartaActual][cartaActual+"-letraOcho"].value;
    if(letraUno == 'b' && letraDos == 'a' && letraTres == 'rr' && letraCuatro == 'i' && letraCinco == 'l' && letraSeis == 'e' && letraSiete == 't' && letraOcho == 'e'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirGusano(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    let letraCinco = document.forms["form"+cartaActual][cartaActual+"-letraCinco"].value;
    let letraSeis = document.forms["form"+cartaActual][cartaActual+"-letraSeis"].value;
    if(letraUno == 'g' && letraDos == 'u' && letraTres == 's' && letraCuatro == 'a' && letraCinco == 'n' && letraSeis == 'o'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirGallina(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    let letraCinco = document.forms["form"+cartaActual][cartaActual+"-letraCinco"].value;
    let letraSeis = document.forms["form"+cartaActual][cartaActual+"-letraSeis"].value;
    if(letraUno == 'g' && letraDos == 'a' && letraTres == 'll' && letraCuatro == 'i' && letraCinco == 'n' && letraSeis == 'a'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}

function corregirGota(){
    let letraUno = document.forms["form"+cartaActual][cartaActual+"-letraUno"].value;
    let letraDos = document.forms["form"+cartaActual][cartaActual+"-letraDos"].value;
    let letraTres = document.forms["form"+cartaActual][cartaActual+"-letraTres"].value;
    let letraCuatro = document.forms["form"+cartaActual][cartaActual+"-letraCuatro"].value;
    if(letraUno == 'g' && letraDos == 'o' && letraTres == 't' && letraCuatro == 'a'){
        correcto();
        continuar();
    } else {
        AlertError();
    }
}