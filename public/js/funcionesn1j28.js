var cartaActual;
var cartas =  [];
var numeroActual;
var posicionActual = 1;
var contadorGlobal = 1;
var restantes = 1;
var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");

var arreglo = ["galleta", "goma", "jirafa", "galera", "joyas", "jinete"];

window.onload = function() {
    cartas.push('galleta');
    cartas.push('goma');
    cartas.push('jirafa');
    cartas.push('galera');
    cartas.push('joyas');
    cartas.push('jinete');
}

async function cargarCartas(){
    await new Promise(resolve => setTimeout(resolve, 10000));
    for(i=0;i<6;i++){
        const selector = document.getElementById(arreglo[i]);
        selector.classList.add('magictime', 'tinUpIn');
        document.getElementById(arreglo[i]).style.display = 'inline-block';
        playSound('../sonidos/'+arreglo[i]+'.wav')
        await new Promise(resolve => setTimeout(resolve, 3000));
    }

    document.getElementById("comenzar").style.display = 'block';
}

function prepareSpelling(){
    document.getElementById("spelling").innerHTML =
        '<div class="col-2" style="border-style: dashed; margin-right: 5px; width: 300px">' +
        '   <input id="autocompleteinput" type="text" style="font-size: 50px; text-transform: uppercase; text-align: center" disabled>' +
        '</div>';
    document.getElementById('autocompleteinput').value=cartaActual.substring(0, posicionActual);
}

function getRandomArbitrary() {
    numeroActual = Math.floor(Math.random() * (6-restantes));
    return numeroActual;
}

function corregir(carta){
    if(carta == cartaActual){
        correcto();
        posicionActual = 1;
        contadorGlobal++;
        cartas.splice(numeroActual, 1);
        if(contadorGlobal <= 4){
            comenzar();
            restantes++
        } else {
            confirmarFinal()
        }
    } else {
        posicionActual++
        incorrecto();
        prepareSpelling()
    }
}

function comenzar(){
    cartaActual = cartas[getRandomArbitrary()];
    document.getElementById('spelling').style.display = 'inline-block';
    document.getElementById('comenzar').style.display = 'none';
    prepareSpelling();
    // playSound('../sonidos/lentos/'+cartaActual+'.wav')
}

function correcto(s) {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
    return false;
}

function incorrecto() {
    //un alert
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
    );
}