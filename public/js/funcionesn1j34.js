var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var pintado = false;
let arreglo = ["bodega", "bajada", "gotera", "bocado"];
var indexGeneral = 0;
var palabraActual = '';
var primeraVez = true;

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
}

function confirmar2() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/nivel1.html";
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function faltanimg() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h1><b> &iexcl; ALGO NO ESTA BIEN ! <br> &iexcl; INTENTALO DE NUEVO ! </b></h1>", function() {
        //aqui introducimos lo que haremos tras cerrar la alerta.
    });
}

function checkTableGameExtraJ6(){
    let tabla = $('#tabla-'+palabraActual);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;


    for (let i = 0; i < items.length; i++) {

        if (items[i].dataset.letra == palabraActual.charAt(i)){
            cont++;
        }
        if (items[i].dataset.letra != palabraActual.charAt(i)){
            alerta();
            return false;
        }

    }

    $('.item').draggable({
        helper: 'clone'
    });

    $('.box-image-rayitas').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);
        }
    });
    if(cont==6){
        document.getElementById(palabraActual).style.display = 'none';
        listadoPalabras(palabraActual, 'none', 'img-');
        listadoPalabras(palabraActual, 'none', '');
        return true;
    } else {
        return false;
    }
}

function checkGame(){
    let a = checkTableGameExtraJ6()
    if(a){
        if(arreglo.length==0){
            confirmar2()
        } else {
            confirmar();
            document.getElementById("divSobre").style.display = "inline-block";
            document.getElementById("divTable").style.display = "none";
            document.getElementById("divCajitas").style.display = "none";
        }
    }
}

async function generarJuego(palabra){
    document.getElementById(palabra).style.display = 'inline-block';
    listadoPalabras(palabra, 'inline-block', '')
}

function listadoPalabras(palabra, directiva, string){
    document.getElementById('tabla-'+palabra).style.display = directiva;
    switch (palabra){
        case 'bocado':
        document.getElementById(string+'bocado-b').style.display = directiva;
        document.getElementById(string+'bocado-o').style.display = directiva;
        document.getElementById(string+'bocado-c').style.display = directiva;
        document.getElementById(string+'bocado-a').style.display = directiva;
        document.getElementById(string+'bocado-d').style.display = directiva;
        document.getElementById(string+'bocado-o2').style.display = directiva;
        break;

        case 'gotera':
        document.getElementById(string+'gotera-g').style.display = directiva;
        document.getElementById(string+'gotera-o').style.display = directiva;
        document.getElementById(string+'gotera-t').style.display = directiva;
        document.getElementById(string+'gotera-e').style.display = directiva;
        document.getElementById(string+'gotera-r').style.display = directiva;
        document.getElementById(string+'gotera-a').style.display = directiva;
        break;

        case 'bodega':
        document.getElementById(string+'bodega-b').style.display = directiva;
        document.getElementById(string+'bodega-o').style.display = directiva;
        document.getElementById(string+'bodega-d').style.display = directiva;
        document.getElementById(string+'bodega-e').style.display = directiva;
        document.getElementById(string+'bodega-g').style.display = directiva;
        document.getElementById(string+'bodega-a').style.display = directiva;
        break;

        case 'bajada':
        document.getElementById(string+'bajada-b').style.display = directiva;
        document.getElementById(string+'bajada-a').style.display = directiva;
        document.getElementById(string+'bajada-j').style.display = directiva;
        document.getElementById(string+'bajada-a2').style.display = directiva;
        document.getElementById(string+'bajada-d').style.display = directiva;
        document.getElementById(string+'bajada-a3').style.display = directiva;
        break;
    }
}


function funcionGeneral(){
    document.getElementById("divSobre").style.display = "none";
    document.getElementById("divTable").style.display = "inline-block";
    document.getElementById("divCajitas").style.display = "flex";
    if(arreglo.length>=1){
        var numeroRandom = Math.floor(Math.random() * arreglo.length);
        if(primeraVez){
            palabraAnterior = arreglo[numeroRandom];
            primeraVezVez = false;
        } else {
            palabraAnterior = palabraActual;
        }
        palabraActual = arreglo[numeroRandom];
        arreglo.splice(numeroRandom,1);
        generarJuego(palabraActual);
    }  else {
        alert('Terminó el juego')
    }
}