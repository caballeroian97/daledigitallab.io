var cartas = ["sonar", "soñar", "nieto", "nieve", "caño", "baño"]
var cartaActual = 'nieto';
var numero = 1;
var left = 6


function sacarCarta() {
    cartaActual = cartas[getRandomArbitrary()];
    numero++
    showForm();
}

function showForm(){
    document.getElementById('container-'+cartaActual).style.display = 'block';
    document.getElementById('imagen'+cartaActual).style.display = 'block';

    document.getElementById('carta').style.display = 'none';

    playSound('../sonidos/'+cartaActual+'.wav')
}

function getRandomArbitrary() {
    numeroActual = Math.floor(Math.random() * left);
    return numeroActual;
}

function continuar(){
    document.getElementById('container-'+cartaActual).setAttribute('style', 'display:none !important');
    document.getElementById('imagen'+cartaActual).setAttribute('style', 'display:none !important');

    document.getElementById('carta').style.display = 'block';

    cartas.splice(cartas.indexOf(cartaActual), 1)
    left--
    if(numero == 6){
        confirmarFinal()
    }
}

function correcto() {
    document.getElementById('container-'+cartaActual).style.display = 'none'
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
    return false;
}

function corregir(){
    window['corregir'+cartaActual]();
}

function corregirnieve(){
    if (document.getElementById('nieve1').children[0].id == 'nieve-N' &
        document.getElementById('nieve2').children[0].id == 'nieve-I' &
        (document.getElementById('nieve3').children[0].id == 'nieve-E' || document.getElementById('nieve4').children[0].id == 'nieve-E-2') &
        document.getElementById('nieve4').children[0].id == 'nieve-V' &
        (document.getElementById('nieve5').children[0].id == 'nieve-E' || document.getElementById('nieve5').children[0].id == 'nieve-E-2')
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar();
    }
}

function corregirsonar(){
    if (document.getElementById('sonar1').children[0].id == 'sonar-S' &
        document.getElementById('sonar2').children[0].id == 'sonar-O' &
        document.getElementById('sonar3').children[0].id == 'sonar-N' &
        document.getElementById('sonar4').children[0].id == 'sonar-A' &
        document.getElementById('sonar5').children[0].id == 'sonar-R'
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar()
    }
}

function corregirnieto() {
    if (document.getElementById('nieto1').children[0].id == 'nieto-N' &
        document.getElementById('nieto2').children[0].id == 'nieto-I' &
        document.getElementById('nieto3').children[0].id == 'nieto-E' &
        document.getElementById('nieto4').children[0].id == 'nieto-T' &
        document.getElementById('nieto5').children[0].id == 'nieto-O'
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar();
    }

}

function corregirsoñar(){
    if (document.getElementById('soñar1').children[0].id == 'soñar-S' &
        document.getElementById('soñar2').children[0].id == 'soñar-O' &
        document.getElementById('soñar3').children[0].id == 'soñar-Ñ' &
        document.getElementById('soñar4').children[0].id == 'soñar-A' &
        document.getElementById('soñar5').children[0].id == 'soñar-R'
    ){
        correcto()
        continuar();
    } else {
        volverASuLugar();
    }
}

function corregircaño(){
    if (document.getElementById('caño1').children[0].id == 'caño-C' &
        document.getElementById('caño2').children[0].id == 'caño-A' &
        document.getElementById('caño3').children[0].id == 'caño-Ñ' &
        document.getElementById('caño4').children[0].id == 'caño-O'
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar();
    }
}

function corregirbaño(){
    if (document.getElementById('baño1').children[0].id == 'baño-B' &
        document.getElementById('baño2').children[0].id == 'baño-A' &
        document.getElementById('baño3').children[0].id == 'baño-Ñ' &
        document.getElementById('baño4').children[0].id == 'baño-O'
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar();
    }
}

function volverASuLugar(){
    for(i=1; i<= cartaActual.length; i++){
        hijo = document.getElementById(cartaActual+i).children[0]
        padre = document.getElementById(cartaActual+'-pos-'+i);
        padre.appendChild(hijo);
    }
    alerta();
}

window.addEventListener("error", function (e) {
    alerta();
})

function alerta() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h4> &iexcl; INTENTALO DE NUEVO !</h4>", function() {
    });
}