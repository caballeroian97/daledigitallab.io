var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var contador = 0;

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/j6_1.html"; //Pasa al siguiente juego
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function siguiente(){
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
        function () {
            $('#tacho').css("display", "block");
            $('#mesa').css("display", "none");
        }
    );
    return false
}

function faltanimg() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h1><b> &iexcl; ALGO NO ESTA BIEN ! <br> &iexcl; INTENTALO DE NUEVO ! </b></h1>", function() {
        //aqui introducimos lo que haremos tras cerrar la alerta.
    });
}

function checkTableGameExtraJ6(tablaId, letras){
    let tabla = $('#' + tablaId);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;
    let padre;
    let hijo;


    for (let i = 0; i < items.length; i++) {

        if (items[i].dataset.letra == letras[i]){
            cont++;
        }
        if (items[i].dataset.letra != letras[i]){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }


    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
        helper: 'clone'
    });

    $('.box-image-rayitas').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);
        }
    });
    return (cont==4);

}

function checkGameExtra(s){
    let a = checkTableGameExtraJ6('tabla1',['m','a','n','o'])
    let b = checkTableGameExtraJ6('tabla2',['m','o','n','o'])
    let c = checkTableGameExtraJ6('tabla3',['m','e','s','a'])
    if(a && b && c){
        confirmar(s)
    }else{
        alerta()
    }
}

function addToTheTable($item, element) {
    if(estaBien($item[0].id)){
        $item.fadeOut(function () {
            $item.appendTo(element).fadeIn(function () {
                $item
                    .animate({
                        height: "65px",
                        width: "75px"
                    })
            });
        });
        $item.detach()
        return true;
    } else {
        return false;
    }
}

function estaBien(palabra){
    let letra = palabra.charAt(0);
    if(letra != 't'){
        siguiente();
        continuar();
        return true;
    } else {
        alerta();
        return false;
    }
}

async function continuar(){
    await new Promise(resolve => setTimeout(resolve, 2000)); 
    if(contador == 0){
        document.getElementById('tiza').style.display = 'none';
        document.getElementById('tela').style.display = 'none';
        document.getElementById('sapo').style.display = 'inline-block';
        document.getElementById('tele').style.display = 'inline-block';
        document.getElementById('tomate').style.display = 'inline-block';
    }
    if(contador == 1){
        document.getElementById('tele').style.display = 'none';
        document.getElementById('tomate').style.display = 'none';
        document.getElementById('pez').style.display = 'inline-block';
        document.getElementById('torta').style.display = 'inline-block';
        document.getElementById('tenedor').style.display = 'inline-block';
    }
    if(contador == 2){
        alertify.success("CARGANDO...");
        await new Promise(resolve => setTimeout(resolve, 2000));
        window.location.href = "../html/" + "n1j14tela" + ".html";
    }
    contador++;
}

function corregirTela(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 't' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 'l' && letraCuatro.toLowerCase() == 'a'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function corregirTiza(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 't' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'z' && letraCuatro.toLowerCase() == 'a'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function corregirTele(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 't' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 'l' && letraCuatro.toLowerCase() == 'e'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function corregirTomate(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    let letraCinco = document.forms["formFoca"]["letraCinco"].value;
    let letraSeis = document.forms["formFoca"]["letraSeis"].value;
    if(letraUno.toLowerCase() == 't' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 'm' && letraCuatro.toLowerCase() == 'a' && letraCinco.toLowerCase() == 't' && letraSeis.toLowerCase() == 'e'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function confirmar(s) {
    sndOK.play();
    alertify.alert(
      "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
      function () {
          alertify.success("CARGANDO...");
          setTimeout(function () {
            window.location.href = "../html/" + s + ".html"; //Pasa al siguiente juego
          }, 1300);
      }
    );
    return false;
}

function clickNumero(numero){
    var foca = 'foca';
    if(foca.length == numero){
        document.getElementById('formFoca').style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function clickNumero6(numero){
    var foca = 'tomate';
    if(foca.length == numero){
        document.getElementById('formFoca').style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function escribirSeguido(p){
    if(p=="letraUno"){
        document.getElementById("letraDos").focus();
    } else if(p=="letraDos"){
        document.getElementById("letraTres").focus();
    } else if(p=="letraTres"){
        document.getElementById("letraCuatro").focus();
    } else if(p=="letraCuatro"){
        document.getElementById("letraCinco").focus();
    } else if(p=="letraCinco"){
        document.getElementById("letraSeis").focus();
    }
}