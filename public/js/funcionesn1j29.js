var cartas = ["helado", "chupete", "chino", "chivo", "hoja", "hilo"]
var cartaActual = 'chino';
var numero = 1;
var left = 6

function sacarCarta() {
    cartaActual = cartas[getRandomArbitrary()];
    numero++
    showForm();
}

function showForm(){
    document.getElementById('container-'+cartaActual).style.display = 'block';
    document.getElementById('imagen'+cartaActual).style.display = 'block';

    document.getElementById('carta').style.display = 'none';

    playSound('../sonidos/'+cartaActual+'.wav')
}

function getRandomArbitrary() {
    numeroActual = Math.floor(Math.random() * left);
    return numeroActual;
}

function continuar(){
    document.getElementById('container-'+cartaActual).setAttribute('style', 'display:none !important');
    document.getElementById('imagen'+cartaActual).setAttribute('style', 'display:none !important');

    document.getElementById('carta').style.display = 'block';

    cartas.splice(cartas.indexOf(cartaActual), 1)
    left--
    if(numero == 6){
        confirmarFinal()
    }
}

function correcto() {
    document.getElementById('container-'+cartaActual).style.display = 'none'
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
    return false;
}

function corregir(){
    window['corregir'+cartaActual]();
}

function corregirchivo(){
    if (document.getElementById('chivo1').children[0].id == 'chivo-CH' &
        document.getElementById('chivo2').children[0].id == 'chivo-I' &
        document.getElementById('chivo3').children[0].id == 'chivo-V' &
        document.getElementById('chivo4').children[0].id == 'chivo-O'
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar();
    }
}

function corregirhelado(){
    if (document.getElementById('helado1').children[0].id == 'helado-H' &
        document.getElementById('helado2').children[0].id == 'helado-E' &
        document.getElementById('helado3').children[0].id == 'helado-L' &
        document.getElementById('helado4').children[0].id == 'helado-A' &
        document.getElementById('helado5').children[0].id == 'helado-D' &
        document.getElementById('helado6').children[0].id == 'helado-O'
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar()
    }
}

function corregirchino() {
    if (document.getElementById('chino1').children[0].id == 'chino-CH' &
        document.getElementById('chino2').children[0].id == 'chino-I' &
        document.getElementById('chino3').children[0].id == 'chino-N' &
        document.getElementById('chino4').children[0].id == 'chino-O'
    ){
        correcto();
        continuar();
    } else {
       volverASuLugar();
    }

}

function corregirchupete(){
    if (document.getElementById('chupete1').children[0].id == 'chupete-CH' &
        document.getElementById('chupete2').children[0].id == 'chupete-U' &
        document.getElementById('chupete3').children[0].id == 'chupete-P' &
        (document.getElementById('chupete4').children[0].id == 'chupete-E' || document.getElementById('chupete4').children[0].id == 'chupete-E-2') &
        document.getElementById('chupete5').children[0].id == 'chupete-T' &
        (document.getElementById('chupete6').children[0].id == 'chupete-E' || document.getElementById('chupete6').children[0].id == 'chupete-E-2')
    ){
        correcto()
        continuar();
    } else {
        volverASuLugar();
    }
}

function corregirhoja(){
    if (document.getElementById('hoja1').children[0].id == 'hoja-H' &
        document.getElementById('hoja2').children[0].id == 'hoja-O' &
        document.getElementById('hoja3').children[0].id == 'hoja-J' &
        document.getElementById('hoja4').children[0].id == 'hoja-A'
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar();
    }
}

function corregirhilo(){
    if (document.getElementById('hilo1').children[0].id == 'hilo-H' &
        document.getElementById('hilo2').children[0].id == 'hilo-I' &
        document.getElementById('hilo3').children[0].id == 'hilo-L' &
        document.getElementById('hilo4').children[0].id == 'hilo-O'
    ){
        correcto();
        continuar();
    } else {
        volverASuLugar();
    }
}

function volverASuLugar(){
    for(i=1; i<= cartaActual.length; i++){
        hijo = document.getElementById(cartaActual+i).children[0]
        padre = document.getElementById(cartaActual+'-pos-'+i);
        padre.appendChild(hijo);
    }
    alerta();
}

window.addEventListener("error", function (e) {
    alerta();
})

function alerta() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h4> &iexcl; INTENTALO DE NUEVO !</h4>", function() {
    });
}


