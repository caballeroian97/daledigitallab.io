var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var pintado = false;
let arreglo = ["pisa", "soda", "cama", "roca"];
var indexGeneral = 0;
var palabraActual = '';
var primeraVez = true;
var esInstruccionEspecial=false;

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
}

function confirmar2() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/nivel1.html";
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function faltanimg() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h1><b> &iexcl; ALGO NO ESTA BIEN ! <br> &iexcl; INTENTALO DE NUEVO ! </b></h1>", function() {
        //aqui introducimos lo que haremos tras cerrar la alerta.
    });
}

function checkTableGameExtraJ6(){
    let tabla = $('#tabla-'+palabraActual);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;

    if(palabraActual != "pista" && palabraActual != "sorda" && palabraActual != "calma" && palabraActual != "ronca"){
        for (let i = 0; i < items.length; i++) {

            if (items[i].dataset.letra == palabraActual.charAt(i)){
                cont++;
            }
            if (items[i].dataset.letra != palabraActual.charAt(i)){
                alerta();
                return false;
            }
        }
    } else {
        let items = document.getElementById(palabraActual+'-correcta');
        if (items.children.length == 0){
            alerta();
            return false;
        }
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
        helper: 'clone'
    });

    $('.box-image-rayitas').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);
        }
    });
    if(palabraActual != "pista" && palabraActual != "sorda" && palabraActual != "calma" && palabraActual != "ronca"){
        if(cont==palabraActual.length){
            document.getElementById(palabraActual).style.display = 'none';
            listadoPalabras(palabraActual, 'none', 'img-');
            listadoPalabras(palabraActual, 'none', '');

            return true;
        } else {
            alerta();
            return false;
        }
    } else {
        document.getElementById(palabraActual).style.display = 'none';
        listadoPalabras(palabraActual, 'none', 'img-');
        listadoPalabras(palabraActual, 'none', '');

        return true;
    }
}

function checkGame(){
    let a = checkTableGameExtraJ6()
    if(palabraActual == "pista" || palabraActual == "sorda" || palabraActual == "calma" || palabraActual == "ronca"){
        if(a){
            if(arreglo.length==0){
                confirmar2()
            } else {
                confirmar();
                document.getElementById("divSobre").style.display = "inline-block";
                document.getElementById("divTable").style.display = "none";
                document.getElementById("divCajitas").style.display = "none";
                esInstruccionEspecial = false;
            }
        }
    } else {
        if(a){
            confirmar();
            funcionGeneral()
            esInstruccionEspecial = true;
        }
    }

}

async function generarJuego(palabra){
    document.getElementById(palabra).style.display = 'inline-block';
    listadoPalabras(palabra, 'inline-block', '')
}

function listadoPalabras(palabra, directiva, string){
    document.getElementById('tabla-'+palabra).style.display = directiva;
    switch (palabra){
        case 'pisa':
        document.getElementById(string+'pisa-p').style.display = directiva;
        document.getElementById(string+'pisa-i').style.display = directiva;
        document.getElementById(string+'pisa-s').style.display = directiva;
        document.getElementById(string+'pisa-a').style.display = directiva;
        break;

        case 'pista':
        document.getElementById(string+'pista-t').style.display = directiva;
        break;

        case 'soda':
        document.getElementById(string+'soda-s').style.display = directiva;
        document.getElementById(string+'soda-o').style.display = directiva;
        document.getElementById(string+'soda-d').style.display = directiva;
        document.getElementById(string+'soda-a').style.display = directiva;
        break;

        case 'sorda':
        document.getElementById(string+'sorda-r').style.display = directiva;
        break;

        case 'cama':
        document.getElementById(string+'cama-c').style.display = directiva;
        document.getElementById(string+'cama-a').style.display = directiva;
        document.getElementById(string+'cama-m').style.display = directiva;
        document.getElementById(string+'cama-a2').style.display = directiva;
        break;

        case 'calma':
        document.getElementById(string+'calma-l').style.display = directiva;
        break;

        case 'roca':
        document.getElementById(string+'roca-r').style.display = directiva;
        document.getElementById(string+'roca-o').style.display = directiva;
        document.getElementById(string+'roca-c').style.display = directiva;
        document.getElementById(string+'roca-a').style.display = directiva;
        break;

        case 'ronca':
        document.getElementById(string+'ronca-n').style.display = directiva;
        break;
    }
}
function funcionGeneral(){
    document.getElementById("divSobre").style.display = "none";
    document.getElementById("divTable").style.display = "inline-block";
    document.getElementById("divCajitas").style.display = "flex";
    var numeroRandom = Math.floor(Math.random() * arreglo.length);
    if(primeraVez){
        palabraAnterior = arreglo[numeroRandom];
        primeraVez = false;
    } else {
        palabraAnterior = palabraActual;
    }

    if(palabraActual == "pisa"){
        palabraActual = "pista";
    } else if(palabraActual == "soda"){
        palabraActual = "sorda";
    } else if(palabraActual == "cama"){
        palabraActual = "calma";
    } else if(palabraActual == "roca"){
        palabraActual = "ronca";
    } else {
        palabraActual = arreglo[numeroRandom];
        arreglo.splice(numeroRandom,1);
    }

    generarJuego(palabraActual);
}

function reproducirInstruccion(sonido){
    if(esInstruccionEspecial){
        reproducirSonido('mueve'+palabraActual+'.wav')
    } else {
        reproducirSonido(sonido)
    }
}
