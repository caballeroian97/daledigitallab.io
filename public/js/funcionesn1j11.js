//nivel 1 juego 11
var sndNO = new Audio("../sonidos/error.wav");
var sndOK = new Audio("../sonidos/ganaste.wav");
let cartasNino = ["../img/niveles/jugo.png", "../img/niveles/semaforo.png", "../img/niveles/jardinero.png", "../img/niveles/linterna.png", "../img/niveles/nido.png"];
let cartasTina = ["../img/niveles/jabon.png", "../img/niveles/lapiz.png", "../img/niveles/jamon.png", "../img/niveles/jarra.png", "../img/niveles/nudo.png", "../img/niveles/sombrero.png"];
let sonidosNino = ["../sonidos/lentos/jugo.wav", "../sonidos/lentos/semaforo.wav", "../sonidos/lentos/jardinero.wav", "../sonidos/lentos/linterna.wav", "../sonidos/lentos/nido.wav"]
let sonidosTina = ["../sonidos/lentos/jabon.wav", "../sonidos/lentos/lapiz.wav", "../sonidos/lentos/jamon.wav", "../sonidos/lentos/jarra.wav", "../sonidos/lentos/nudo.wav", "../sonidos/lentos/sombrero.wav"]
var posicionNino = 0;
var puntaje = 0;
var terminoElJuego = false;

/*Nivel 1 juego 11*/

function reproducirSonido(s) {
    var sonido = new Audio("../sonidos/" + s);
    sonido.play();
}

async function sacoCartaYo() {
    const selector = document.querySelector('.imagenNino')
    if(selector.style.display == 'none'){
        verificarTerminoJuego()
        if(!terminoElJuego){
            
            selector.classList.add('magictime', 'tinUpIn')
            selector.style.display = 'block';
            posicionNino = Math.floor(Math.random() * cartasNino.length);
            document.getElementById('cartaNino').src=cartasNino[posicionNino];
            reproducirSonido(sonidosNino[posicionNino]);
            sacaCartaTina();
        }
    }
}

async function sacaCartaTina() {
    await new Promise(resolve => setTimeout(resolve, 8000));
    const selector = document.querySelector('.imagenTina')
    selector.classList.add('magictime', 'tinUpIn')
    document.getElementById('imagenTina').style.display = 'block';
    document.getElementById('cartaTina').src=cartasTina[0];
    reproducirSonido(sonidosTina[0]);
}

async function verificarIguales(){
    let selector = document.querySelector('.imagenNino')
    if(!terminoElJuego && selector.style.display == 'block'){
        let letra1 = cartasNino[posicionNino]
        let letra2 = cartasTina[0]
        if(letra1.slice(15,16) == letra2.slice(15,16)){
            puntaje++;
            document.getElementById("puntaje").innerHTML = puntaje;
            document.getElementById('imagenTina').classList.remove("magictime")
            document.getElementById('imagenTina').classList.remove("tinUpIn")
            document.getElementById('imagenNino').classList.remove("magictime")
            document.getElementById('imagenNino').classList.remove("tinUpIn")
            selector = document.querySelector('.imagenTina')
            selector.classList.add('magictime', 'bombLeftOut')
            selector = document.querySelector('.imagenNino')
            selector.classList.add('magictime', 'bombRightOut')

            cartasNino.splice(posicionNino,1);
            sonidosNino.splice(posicionNino,1);
            cartasTina.shift();
            sonidosTina.shift();

            verificarTerminoJuego()
            if(!terminoElJuego){
                confirmar()
            }
        } else {
            incorrecto()
        }
    }
}

async function verificarDesiguales(){
    let selector = document.querySelector('.imagenNino')
    if(!terminoElJuego && selector.style.display == 'block'){
        let letra1 = cartasNino[posicionNino]
        let letra2 = cartasTina[0]
        if(letra1.slice(15,16) != letra2.slice(15,16)){
            document.getElementById('imagenTina').classList.remove("magictime")
            document.getElementById('imagenTina').classList.remove("tinUpIn")
            document.getElementById('imagenNino').classList.remove("magictime")
            document.getElementById('imagenNino').classList.remove("tinUpIn")
            selector = document.querySelector('.imagenTina')
            selector.classList.add('magictime', 'tinUpOut')
            selector = document.querySelector('.imagenNino')
            selector.classList.add('magictime', 'tinUpOut')

            var cartaNinoAlFondo = cartasNino.splice(posicionNino,1)[0];
            var sonidoNinoAlFondo = sonidosNino.splice(posicionNino,1)[0];
            var cartaTinaAlFondo = cartasTina.shift();
            var sonidoTinaAlFondo = sonidosTina.shift();

            cartasNino.push(cartaNinoAlFondo);
            sonidosNino.push(sonidoNinoAlFondo);
            cartasTina.push(cartaTinaAlFondo);
            sonidosTina.push(sonidoTinaAlFondo);

            confirmarDesigual()
        } else {
            incorrecto()
        }
    }
}

async function incorrecto(){
    document.getElementById('imagenTina').classList.remove("magictime")
    document.getElementById('imagenTina').classList.remove("tinUpIn")
    document.getElementById('imagenNino').classList.remove("magictime")
    document.getElementById('imagenNino').classList.remove("tinUpIn")
    let selector = document.querySelector('.imagenTina')
    selector.classList.add('magictime', 'tinUpOut')
    selector = document.querySelector('.imagenNino')
    selector.classList.add('magictime', 'tinUpOut')

    var cartaNinoAlFondo = cartasNino.splice(posicionNino,1)[0];
    var sonidoNinoAlFondo = sonidosNino.splice(posicionNino,1)[0];
    var cartaTinaAlFondo = cartasTina.shift();
    var sonidoTinaAlFondo = sonidosTina.shift();

    cartasNino.push(cartaNinoAlFondo);
    sonidosNino.push(sonidoNinoAlFondo);
    cartasTina.push(cartaTinaAlFondo);
    sonidosTina.push(sonidoTinaAlFondo);

    confirmarIncorrecto()
}

window.onload = function(){
    document.getElementById("puntaje").innerHTML = puntaje;
};

async function confirmar() {
    sndOK.play();
    await alertify.alert("<img src='../assets/feliz.png'> <h3>&iexcl; SUMAS UN PUNTO, SACA OTRA CARTA !</h3>");
    await new Promise(resolve => setTimeout(resolve, 1000));
    document.getElementById('imagenTina').classList.remove("magictime")
    document.getElementById('imagenTina').classList.remove("bombLeftOut")
    document.getElementById('imagenNino').classList.remove("magictime")
    document.getElementById('imagenNino').classList.remove("bombRightOut")
    document.getElementById('imagenTina').style.display = 'none';
    document.getElementById('imagenNino').style.display = 'none';
    return false;
}

async function confirmarDesigual() {
    sndOK.play();
    await alertify.alert("<img src='../assets/feliz.png'> <h3>&iexcl; SACA OTRA CARTA !</h3>");
    await new Promise(resolve => setTimeout(resolve, 1000));
    document.getElementById('imagenTina').classList.remove("magictime")
    document.getElementById('imagenTina').classList.remove("tinUpOut")
    document.getElementById('imagenNino').classList.remove("magictime")
    document.getElementById('imagenNino').classList.remove("tinUpOut")
    document.getElementById('imagenTina').style.display = 'none';
    document.getElementById('imagenNino').style.display = 'none';
    return false;
}

  async function confirmarIncorrecto() {
    sndNO.play();
    await alertify.alert("<img src='../assets/triste.png'> <h3>&iexcl; TE EQUIVOCASTE, SACA OTRA CARTA !</h3>");
    await new Promise(resolve => setTimeout(resolve, 1000));
    document.getElementById('imagenTina').classList.remove("magictime")
    document.getElementById('imagenTina').classList.remove("tinUpOut")
    document.getElementById('imagenNino').classList.remove("magictime")
    document.getElementById('imagenNino').classList.remove("tinUpOut")
    document.getElementById('imagenTina').style.display = 'none';
    document.getElementById('imagenNino').style.display = 'none';
    return false;
}

function verificarTerminoJuego(){
    if(cartasNino.length < 1){
        terminoElJuego = true;
        confirmarFinal();
    }
}