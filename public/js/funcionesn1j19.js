let pintado = false; // En el juego , si se selecciona una imagen esta var se pone en true
let letraSelec = null;

var cartasPaco = [];
var arreglo = ["gato", "gota", "carpa", "copa", "gorro", "gusano", "cama", "corazon", "bote", "pantalon"]
var cantidadCombinacionesPaco = 0;
var cantidadCombinacionesTina = 0;
var letras = ["a", "b", "c", "d", "e"];

async function repartir(){
    document.getElementById('tablaComun').style.display = 'flex';
    document.getElementById('cartelRepartir').style.display = 'none'
    for(i=0;i<5;i++){
        var randomNumber = Math.floor(Math.random()*arreglo.length)
        cartasPaco.push(arreglo[randomNumber])
        arreglo.splice(randomNumber, 1);
    }

    await cargarCartasPaco(false);
    await cargarCartasTina(false);


    playSound('../sonidos/Instrucciones19-2.wav')

    let copiaCartasPaco = [];
    cartasPaco.forEach(value => copiaCartasPaco.push(value))
    for(i=0; i < copiaCartasPaco.length; i++){
        j = 0;
        ok = false;
        while (j < copiaCartasPaco.length && !ok) {
            if (copiaCartasPaco[i].charAt(0) === copiaCartasPaco[j].charAt(0) && copiaCartasPaco[i] !== copiaCartasPaco[j]) {
                cantidadCombinacionesPaco++;
                copiaCartasPaco.splice(i, 1);
                copiaCartasPaco.splice(j-1, 1);
                ok = true;
            }
            j++
        }
    }

    let copiaCartasTina = [];
    arreglo.forEach(value => copiaCartasTina.push(value))
    for(i=0; i < copiaCartasTina.length; i++){
        j = 0;
        ok = false;
        while (j < copiaCartasTina.length && !ok) {
            if (copiaCartasTina[i].charAt(0) === copiaCartasTina[j].charAt(0) && copiaCartasTina[i] !== copiaCartasTina[j]) {
                cantidadCombinacionesTina++;
                copiaCartasTina.splice(i, 1);
                copiaCartasTina.splice(j-1, 1);
                ok = true;
            }
            j++
        }
    }
    setDraggable();
}

function setDraggable(){
    $(".item").draggable({
        helper: "clone",
    });

    $(".box-image2").droppable({
        accept: ".item",
        hoverClass: "hovering",
        drop: function (ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);
        },
    });
}

async function cargarCartasPaco(esReload) {
    for (const value of cartasPaco) {
        const index = cartasPaco.indexOf(value);
        var concatStringLetras = 'paco' + letras[index];
        var concatString = 'paco' + (index + 1).toString()
        var divNode = document.createElement('div')
        divNode.classList.add("item", "box-image2");
        divNode.setAttribute('id', concatString)
        divNode.setAttribute('style', 'border: none')
        var node = document.createElement('img');
        node.src = " ../assets/img/cards/" + cartasPaco[index] + ".png";
        node.classList.add("imgJuegosMini", "slideDown")
        node.setAttribute('id', cartasPaco[index]);
        node.setAttribute('style', 'height:75px; width: 75px');
        document.getElementById(concatStringLetras).appendChild(divNode);
        divNode.appendChild(node);
        if(!esReload){
            playSound('../sonidos/lentos/' + cartasPaco[index] + '.wav')
            await new Promise(resolve => setTimeout(resolve, 4000));
        }
    }
}

async function cargarCartasTina(esReload) {
    for (const value of arreglo) {
        const index = arreglo.indexOf(value);
        var concatStringLetras = 'tina' + letras[index];
        var concatString = 'tina' + (index + 1).toString()
        var divNode = document.createElement('div')
        divNode.classList.add("item", "box-image2");
        divNode.setAttribute('id', concatString)
        divNode.setAttribute('style', 'border: none')
        var node = document.createElement('img');
        node.src = " ../assets/img/cards/" + arreglo[index] + ".png";
        node.classList.add("imgJuegosMini", "slideDown")
        node.setAttribute('id', arreglo[index]);
        node.setAttribute('style', 'height:75px; width: 75px');
        document.getElementById(concatStringLetras).appendChild(divNode);
        divNode.appendChild(node);
        if(!esReload){
            playSound('../sonidos/lentos/' + arreglo[index] + '.wav')
            await new Promise(resolve => setTimeout(resolve, 4000));
        }
    }
}

function limpiarTablas(){
    for(i=1; i <= 4; i++){
        if(document.getElementById('paresPaco'+i).innerHTML)
        document.getElementById('paresPaco'+i).innerHTML = '';
    }

    for(i=1; i <= 4; i++){
        if(document.getElementById('paresTina'+i).innerHTML)
            document.getElementById('paresTina'+i).innerHTML = '';
    }

    for(i=0; i <= letras.length; i++){
        if(document.getElementById('tina'+letras[i]))
            document.getElementById('tina'+letras[i]).innerHTML = '';
    }

    for(i=0; i <= letras.length; i++){
        if(document.getElementById('paco'+letras[i]))
            document.getElementById('paco'+letras[i]).innerHTML = '';
    }

    for(i=1; i <= 2; i++){
        if(document.getElementById('parejas'+[i]).innerHTML)
            document.getElementById('parejas'+[i]).innerHTML = '';
    }

}

async function comprobar(s) {
    if (checkTableP('paresPaco') & checkTableT('paresTina') & checkTablePareja()) {
        confirmar(s);
    } else {
        limpiarTablas();
        await cargarCartasPaco(true)
        await cargarCartasTina(true)
        setDraggable();
        alerta();
    }
}

function checkTableP(personaje) {
    let valorCarta1 = document.getElementById(personaje+'1')?.children[0]?.children[0]?.id
    let valorCarta2 = document.getElementById(personaje+'2')?.children[0]?.children[0]?.id


    if(cartasPaco.includes(valorCarta1) && cartasPaco.includes(valorCarta2) && valorCarta1.charAt(0) === valorCarta2.charAt(0)){
        if(cantidadCombinacionesPaco == 1){
            return true
        }

        let valorCarta3 = document.getElementById(personaje+'3')?.children[0]?.children[0]?.id
        let valorCarta4 = document.getElementById(personaje+'4')?.children[0]?.children[0]?.id


        if(cartasPaco.includes(valorCarta3) && cartasPaco.includes(valorCarta4) && valorCarta3.charAt(0) === valorCarta4.charAt(0)){
            return true
        }
    }

    return false;
}

function checkTableT(personaje) {
    let valorCarta1 = document.getElementById(personaje+'1')?.children[0]?.children[0]?.id
    let valorCarta2 = document.getElementById(personaje+'2')?.children[0]?.children[0]?.id


    if(arreglo.includes(valorCarta1) && arreglo.includes(valorCarta2) && valorCarta1.charAt(0) === valorCarta2.charAt(0)){
        if(cantidadCombinacionesTina == 1){
            return true
        }

        let valorCarta3 = document.getElementById(personaje+'3')?.children[0]?.children[0]?.id
        let valorCarta4 = document.getElementById(personaje+'4')?.children[0]?.children[0]?.id


        if(arreglo.includes(valorCarta3) && arreglo.includes(valorCarta4) && valorCarta3.charAt(0) === valorCarta4.charAt(0)){
            return true
        }
    }

    return false;
}

function checkTablePareja() {
    if(cantidadCombinacionesTina+cantidadCombinacionesPaco == 4){
        return true;
    }

    let parejas1 = document.getElementById('parejas1')?.children[0]?.children[0]?.id
    let parejas2 = document.getElementById('parejas2')?.children[0]?.children[0]?.id


    if(((cartasPaco.includes(parejas1) || arreglo.includes(parejas2)) ||
        (cartasPaco.includes(parejas2) || arreglo.includes(parejas1))) &&
        parejas1.charAt(0) === parejas2.charAt(0)){
        return true;
    }

    return false;
}

function clickNumero(numero){
  if(numero == 4){
      document.getElementById('form15').style.display = 'block';
      document.getElementById('tablero').style.display = 'none';
  } else {
      AlertError();
  }
}

function clickNumero6(numero){
  if(numero == 6){
      document.getElementById('form15').style.display = 'block'
      document.getElementById('tablero').style.display = 'none';
  } else {
      AlertError();
  }
}

function escribirSeguido(p){
  if(p=="letraUno"){
      document.getElementById("letraDos").focus();
  } else if(p=="letraDos"){
      document.getElementById("letraTres").focus();
  } else if(p=="letraTres"){
      document.getElementById("letraCuatro").focus();
  } else if(p=="letraCuatro"){
      document.getElementById("letraCinco").focus();
  } else if(p=="letraCinco"){
    document.getElementById("letraSeis").focus();
  }
}

function escribirSeguidoLetraDoble(p){
  if(document.getElementById(p).value.length > 1){
      if(p=="letraUno"){
          document.getElementById("letraDos").focus();
      } else if(p=="letraDos"){
          document.getElementById("letraTres").focus();
      } else if(p=="letraTres"){
          document.getElementById("letraCuatro").focus();
      } else if(p=="letraCuatro"){
          document.getElementById("letraCinco").focus();
      }
  }
  
}

function corregirGato(p){
  let letraUno = document.forms["form15"]["letraUno"].value;
  let letraDos = document.forms["form15"]["letraDos"].value;
  let letraTres = document.forms["form15"]["letraTres"].value;
  let letraCuatro = document.forms["form15"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'g' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 't' && letraCuatro.toLowerCase() == 'o'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirGota(p){
  let letraUno = document.forms["form15"]["letraUno"].value;
  let letraDos = document.forms["form15"]["letraDos"].value;
  let letraTres = document.forms["form15"]["letraTres"].value;
  let letraCuatro = document.forms["form15"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'g' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 't' && letraCuatro.toLowerCase() == 'a'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirGorro(p){
  let letraUno = document.forms["form15"]["letraUno"].value;
  let letraDos = document.forms["form15"]["letraDos"].value;
  let letraTres = document.forms["form15"]["letraTres"].value;
  let letraCuatro = document.forms["form15"]["letraCuatro"].value;
  if(letraUno.toLowerCase() == 'g' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 'rr' && letraCuatro.toLowerCase() == 'o'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function corregirGusano(p){
  let letraUno = document.forms["form15"]["letraUno"].value;
  let letraDos = document.forms["form15"]["letraDos"].value;
  let letraTres = document.forms["form15"]["letraTres"].value;
  let letraCuatro = document.forms["form15"]["letraCuatro"].value;
  let letraCinco = document.forms["form15"]["letraCinco"].value;
  let letraSeis = document.forms["form15"]["letraSeis"].value;
  if(letraUno.toLowerCase() == 'g' && letraDos.toLowerCase() == 'u' && letraTres.toLowerCase() == 's' && letraCuatro.toLowerCase() == 'a' && letraCinco.toLowerCase() == 'n' && letraSeis.toLowerCase() == 'o'){
      confirmar(p);
  } else {
      AlertError();
  }
}

function checkPalabra(palabra){
  if(palabra == 'garra' || palabra == 'gato'){
      if(palabra == 'garra'){
          estaBien();
          document.getElementById('gorra').style.display = 'none'
          document.getElementById('garra').style.display = 'none'
          document.getElementById('gana').style.display = 'none'
          document.getElementById('imagenGarra').style.display = 'none'
          document.getElementById('gallo').style.display = 'inline-block'
          document.getElementById('gajo').style.display = 'inline-block'
          document.getElementById('gato').style.display = 'inline-block'
          document.getElementById('imagenGato').style.display = 'inline-block'
      }
      if(palabra == 'gato'){
          confirmarFinal();
      }
  } else {
    alerta()
  }
}

function estaBien() {
  sndOK.play();
  alertify.alert( "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>");
}

function alerta() {
  sndNO.play();
  alertify.alert(
      "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
      function () {
          //aqui introducimos lo que haremos tras cerrar la alerta.
      }
  );
}

const quitarAnimacion = () => {
    let element = document.getElementById("ayudabtn");
    element.classList.remove("pulse");
    let ocultarMano = document.getElementById("manoayuda")
    ocultarMano.classList.add("imgOcultar")
}

const ocultarElement = id => {
  let elem = document.getElementById(id)
  elem.classList.add("imgOcultar")
}

const quitarAnim = id => {
  let elem = document.getElementById(id)
  elem.classList.remove("pulse")
}

const mostrarElement = id => {
  let elem = document.getElementById(id)
  elem.classList.remove("imgOcultar")
}

const animar = (id) => {
  let elem = document.getElementById(id)
  elem.classList.add("pulse")
}

function enmarcar(event) {

  animar('j1-btncheck')
  mostrarElement('manoayuda3')

  let element = document.getElementById("manoayuda2")
  element.classList.add("imgOcultar")
 
  let selec = event.target;
  if (pintado == false) {
    selec.className += "cambiarBorde";
    pintado = true;
    letraSelec = selec.id;
   
  } else {
    $(".cambiarBorde").removeClass("cambiarBorde");
    selec.className += " cambiarBorde";
    letraSelec = selec.id;
   
  }
}
var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");

/*Cartelito*/

function confirmar(s) {
  sndOK.play();
  alertify.alert(
    "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    function () {
        alertify.success("CARGANDO...");
        setTimeout(function () {
          window.location.href = "../html/" + s + ".html"; //Pasa al siguiente juego
        }, 1300);
    }
  );
  return false;
}

function alerta() {
  //un alert
  sndNO.play();
  alertify.alert(
    "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
    function () {
      //aqui introducimos lo que haremos tras cerrar la alerta.
    }
  );
}

//JuegoExtra
const checkGameExtra = (s) => {
  let a = checkTableGameExtra('tabla1',['o','l','a'])
  let b = checkTableGameExtra('tabla2',['o','s','o'])
  let c = checkTableGameExtra('tabla3',['u','n','o'])
  if(a && b && c){
    confirmarFinal(s)
  }else{
    alerta()
  }
  
}


const checkTableGameExtra = (tablaId, letras) => {

    let tabla = $('#' + tablaId);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;
    let padre;
    let hijo;
   

    for (let i = 0; i < items.length; i++) {

        if (items[i].dataset.letra == letras[i]){
          cont++;
        }
        if (items[i].dataset.letra != letras[i]){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }
       
        
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
                helper: 'clone'
            });

            $('.box-image-rayitas').droppable({
                accept: '.item',
                hoverClass: 'hovering',
                drop: function(ev, ui) {
                    ui.draggable.detach();
                    $(this).append(ui.draggable);
                }
    });
   return (cont==3);
}

