var arreglo = [];
var arreglo2 = ["sombrero", "foca", "sopa", "ventana", "farol", "cuaderno", "espejo", "foco", "fila", "tortuga", "serpiente", "foto"]


async function autoPlay(){
    await new Promise(resolve => setTimeout(resolve, 15000));
    arreglo["sombrero"] = false;
    arreglo["foca"] = false;
    arreglo["sopa"] = false;
    arreglo["ventana"] = false;
    arreglo["farol"] = false;
    arreglo["cuaderno"] = false;
    arreglo["espejo"] = false;
    arreglo["foco"] = false;
    arreglo["fila"] = false;
    arreglo["tortuga"] = false;
    arreglo["serpiente"] = false;
    arreglo["foto"] = false;
    cargarCartas();
}
window.onload = function(){

};

async function cargarCartas(){
    for(i=0;i<13;i++){
        const selector = document.getElementById(arreglo2[i]);
        selector.classList.add('magictime', 'tinUpIn');
        document.getElementById(arreglo2[i]).style.display = 'inline-block';
        playSound('../sonidos/lentos/'+arreglo2[i]+'.wav')
        await new Promise(resolve => setTimeout(resolve, 5000));
    }
}

function enmarcarVarias(event) {
    var carta = event.target;
    if (arreglo[carta.id] == false) {
        carta.className += " cambiarBorde";
        arreglo[carta.id] = true;
    } else {
        document.getElementById(carta.id).classList.remove("cambiarBorde");
        arreglo[carta.id] = false;
    }
}

function corregir(p){
    if (conF() && sinF()){
        confirmar(p);
    } else {
        AlertError
    }
}

function conF(){
    if (arreglo["foca"] && arreglo["foto"] && arreglo["farol"] && arreglo["foco"] && arreglo["fila"]){
        return true
    } else {
        return false;
    }
}

function sinF(){
    if(!arreglo["serpiente"] && !arreglo["sombrero"] && !arreglo["sopa"] && !arreglo["ventana"] && !arreglo["tortuga"] && !arreglo["cuaderno"] && !arreglo["espejo"]){
        return true;
    } else {
        return false;
    }
}

function clickNumero(numero){
    var foca = 'foca';
    if(foca.length == numero){
        document.getElementById('formFoca').style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}

function clickNumero5(numero){
    var foca = 'farol';
    if(foca.length == numero){
        document.getElementById('formFoca').style.display = 'block'
    } else {
        AlertError();
    }
}

function corregirFoca(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 'f' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 'c' && letraCuatro.toLowerCase() == 'a'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function escribirSeguido(p){
    if(p=="letraUno"){
        document.getElementById("letraDos").focus();
    } else if(p=="letraDos"){
        document.getElementById("letraTres").focus();
    } else if(p=="letraTres"){
        document.getElementById("letraCuatro").focus();
    } else if(p=="letraCuatro"){
        document.getElementById("letraCinco").focus();
    }
}

function corregirFoto(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 'f' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 't' && letraCuatro.toLowerCase() == 'o'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function corregirFarol(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    let letraCinco = document.forms["formFoca"]["letraCinco"].value;
    if(letraUno.toLowerCase() == 'f' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'r' && letraCuatro.toLowerCase() == 'o' && letraCinco.toLowerCase() == 'l'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function corregirFila(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 'f' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'l' && letraCuatro.toLowerCase() == 'a'){
        confirmar(p);
    } else {
        AlertError();
    }
}

function corregirFoco(p){
    let letraUno = document.forms["formFoca"]["letraUno"].value;
    let letraDos = document.forms["formFoca"]["letraDos"].value;
    let letraTres = document.forms["formFoca"]["letraTres"].value;
    let letraCuatro = document.forms["formFoca"]["letraCuatro"].value;
    if(letraUno.toLowerCase() == 'f' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 'c' && letraCuatro.toLowerCase() == 'o'){
        confirmarFinal(p)
    } else {
        AlertError();
    }
}

function confirmar(s) {
    sndOK.play();
    alertify.alert(
      "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
      function () {
          alertify.success("CARGANDO...");
          setTimeout(function () {
            window.location.href = "../html/" + s + ".html"; //Pasa al siguiente juego
          }, 1300);
      }
    );
    return false;
}