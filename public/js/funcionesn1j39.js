var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var pintado = false;
let arreglo = ["capa", "rata", "baba", "taco"];
var indexGeneral = 0;
var palabraActual = '';
var primeraVez = true;
var esInstruccionEspecial=false;

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
}

function confirmar2() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/nivel1.html";
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function faltanimg() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h1><b> &iexcl; ALGO NO ESTA BIEN ! <br> &iexcl; INTENTALO DE NUEVO ! </b></h1>", function() {
        //aqui introducimos lo que haremos tras cerrar la alerta.
    });
}

function checkTableGameExtraJ6(){
    let tabla = $('#tabla-'+palabraActual);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;

    if(palabraActual != "carpa" && palabraActual != "rasta" && palabraActual != "barba" && palabraActual != "talco"){
        for (let i = 0; i < items.length; i++) {

            if (items[i].dataset.letra == palabraActual.charAt(i)){
                cont++;
            }
            if (items[i].dataset.letra != palabraActual.charAt(i)){
                alerta();
                return false;
            }
        }
    } else {
        let items = document.getElementById(palabraActual+'-correcta');
        if (items.children.length == 0){
            alerta();
            return false;
        }
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
        helper: 'clone'
    });

    $('.box-image-rayitas').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);
        }
    });
    if(palabraActual != "carpa" && palabraActual != "rasta" && palabraActual != "barba" && palabraActual != "talco"){
        if(cont==palabraActual.length){
            document.getElementById(palabraActual).style.display = 'none';
            listadoPalabras(palabraActual, 'none', 'img-');
            listadoPalabras(palabraActual, 'none', '');

            return true;
        } else {
            alerta();
            return false;
        }
    } else {
        document.getElementById(palabraActual).style.display = 'none';
        listadoPalabras(palabraActual, 'none', 'img-');
        listadoPalabras(palabraActual, 'none', '');

        return true;
    }
}

function checkGame(){
    let a = checkTableGameExtraJ6()
    if(palabraActual == "carpa" || palabraActual == "rasta" || palabraActual == "barba" || palabraActual == "talco"){
        if(a){
            if(arreglo.length==0){
                confirmar2()
            } else {
                confirmar();
                document.getElementById("divSobre").style.display = "inline-block";
                document.getElementById("divTable").style.display = "none";
                document.getElementById("divCajitas").style.display = "none";
                esInstruccionEspecial = false;
            }
        }
    } else {
        if(a){
            confirmar();
            funcionGeneral();
            esInstruccionEspecial = true;
        }
    }

}

async function generarJuego(palabra){
    document.getElementById(palabra).style.display = 'inline-block';
    listadoPalabras(palabra, 'inline-block', '')
}

function listadoPalabras(palabra, directiva, string){
    document.getElementById('tabla-'+palabra).style.display = directiva;
    switch (palabra){
        case 'capa':
            document.getElementById(string+'capa-c').style.display = directiva;
            document.getElementById(string+'capa-a').style.display = directiva;
            document.getElementById(string+'capa-p').style.display = directiva;
            document.getElementById(string+'capa-a-2').style.display = directiva;
            break;

        case 'carpa':
            document.getElementById(string+'carpa-r').style.display = directiva;
            break;

        case 'rata':
            document.getElementById(string+'rata-r').style.display = directiva;
            document.getElementById(string+'rata-a').style.display = directiva;
            document.getElementById(string+'rata-t').style.display = directiva;
            document.getElementById(string+'rata-a-2').style.display = directiva;
            break;

        case 'rasta':
            document.getElementById(string+'rasta-s').style.display = directiva;
            break;

        case 'baba':
            document.getElementById(string+'baba-b').style.display = directiva;
            document.getElementById(string+'baba-a').style.display = directiva;
            document.getElementById(string+'baba-b-2').style.display = directiva;
            document.getElementById(string+'baba-a-2').style.display = directiva;
            break;

        case 'barba':
            document.getElementById(string+'barba-r').style.display = directiva;
            break;

        case 'taco':
            document.getElementById(string+'taco-t').style.display = directiva;
            document.getElementById(string+'taco-a').style.display = directiva;
            document.getElementById(string+'taco-c').style.display = directiva;
            document.getElementById(string+'taco-o').style.display = directiva;
            break;

        case 'talco':
            document.getElementById(string+'talco-l').style.display = directiva;
            break;
    }
}
function funcionGeneral(){
    document.getElementById("divSobre").style.display = "none";
    document.getElementById("divTable").style.display = "inline-block";
    document.getElementById("divCajitas").style.display = "flex";
    var numeroRandom = Math.floor(Math.random() * arreglo.length);
    if(primeraVez){
        palabraAnterior = arreglo[numeroRandom];
        primeraVez = false;
    } else {
        palabraAnterior = palabraActual;
    }

    if(palabraActual == "capa"){
        palabraActual = "carpa";
    } else if(palabraActual == "rata"){
        palabraActual = "rasta";
    } else if(palabraActual == "baba"){
        palabraActual = "barba";
    } else if(palabraActual == "taco"){
        palabraActual = "talco";
    } else {
        palabraActual = arreglo[numeroRandom];
        arreglo.splice(numeroRandom,1);
    }

    generarJuego(palabraActual);
}

function reproducirInstruccion(sonido){
    if(esInstruccionEspecial){
        reproducirSonido('mueve'+palabraActual+'.wav')
    } else {
        reproducirSonido(sonido)
    }
}