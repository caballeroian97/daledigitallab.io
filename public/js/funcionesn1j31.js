var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var pintado = false;
let arreglo = ["moneda", "minuto", "nevado", "numero"];
var indexGeneral = 0;
var palabraActual = '';
var primeraVez = true;

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
}

function confirmar2() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/nivel1.html";
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function faltanimg() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h1><b> &iexcl; ALGO NO ESTA BIEN ! <br> &iexcl; INTENTALO DE NUEVO ! </b></h1>", function() {
        //aqui introducimos lo que haremos tras cerrar la alerta.
    });
}

function checkTableGameExtraJ6(){
    let tabla = $('#tabla-'+palabraActual);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;


    for (let i = 0; i < items.length; i++) {

        if (items[i].dataset.letra == palabraActual.charAt(i)){
            cont++;
        }
        if (items[i].dataset.letra != palabraActual.charAt(i)){
            alerta();
            return false;
        }

    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
        helper: 'clone'
    });

    $('.box-image-rayitas').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);
        }
    });
    if(cont==6){
        document.getElementById(palabraActual).style.display = 'none';
        listadoPalabras(palabraActual, 'none', 'img-');
        listadoPalabras(palabraActual, 'none', '');
        return true;
    } else {
        return false;
    }
}

function checkGame(){
    let a = checkTableGameExtraJ6()
    if(a){
        if(arreglo.length==0){
            confirmar2()
        } else {
            confirmar();
            document.getElementById("divSobre").style.display = "inline-block";
            document.getElementById("divTable").style.display = "none";
            document.getElementById("divCajitas").style.display = "none";
        }
    }
}

async function generarJuego(palabra){
    document.getElementById(palabra).style.display = 'inline-block';
    listadoPalabras(palabra, 'inline-block', '')
}

function listadoPalabras(palabra, directiva, string){
    document.getElementById('tabla-'+palabra).style.display = directiva;
    switch (palabra){
        case 'numero':
        document.getElementById(string+'numero-n').style.display = directiva;
        document.getElementById(string+'numero-u').style.display = directiva;
        document.getElementById(string+'numero-m').style.display = directiva;
        document.getElementById(string+'numero-e').style.display = directiva;
        document.getElementById(string+'numero-r').style.display = directiva;
        document.getElementById(string+'numero-o').style.display = directiva;
        break;

        case 'nevado':
        document.getElementById(string+'nevado-n').style.display = directiva;
        document.getElementById(string+'nevado-e').style.display = directiva;
        document.getElementById(string+'nevado-v').style.display = directiva;
        document.getElementById(string+'nevado-a').style.display = directiva;
        document.getElementById(string+'nevado-d').style.display = directiva;
        document.getElementById(string+'nevado-o').style.display = directiva;
        break;

        case 'moneda':
        document.getElementById(string+'moneda-m').style.display = directiva;
        document.getElementById(string+'moneda-o').style.display = directiva;
        document.getElementById(string+'moneda-n').style.display = directiva;
        document.getElementById(string+'moneda-e').style.display = directiva;
        document.getElementById(string+'moneda-d').style.display = directiva;
        document.getElementById(string+'moneda-a').style.display = directiva;
        break;

        case 'minuto':
        document.getElementById(string+'minuto-m').style.display = directiva;
        document.getElementById(string+'minuto-i').style.display = directiva;
        document.getElementById(string+'minuto-n').style.display = directiva;
        document.getElementById(string+'minuto-u').style.display = directiva;
        document.getElementById(string+'minuto-t').style.display = directiva;
        document.getElementById(string+'minuto-o').style.display = directiva;
        break;
    }
}

function funcionGeneral(){
    document.getElementById("divSobre").style.display = "none";
    document.getElementById("divTable").style.display = "inline-block";
    document.getElementById("divCajitas").style.display = "flex";
    if(arreglo.length>=1){
        var numeroRandom = Math.floor(Math.random() * arreglo.length);
        if(primeraVez){
            palabraAnterior = arreglo[numeroRandom];
            primeraVezVez = false;
        } else {
            palabraAnterior = palabraActual;
        }
        palabraActual = arreglo[numeroRandom];
        arreglo.splice(numeroRandom,1);
        generarJuego(palabraActual);
    }  else {
        alert('Terminó el juego')
    }
}