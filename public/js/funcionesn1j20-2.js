var cartaActual;
var cartas =  [];
var max = 6;
var numeroActual;



function clickNumero(numero){
    if(cartaActual.length == numero){
        document.getElementById('form'+cartaActual).style.display = 'block'
        document.getElementById('tablero').style.display = 'none';
    } else {
        AlertError();
    }
}


window.onload = function() {
    cartas.push('luna');
    cartas.push('nido');
    cartas.push('loro');
    cartas.push('nena');
    cartas.push('lana');
    cartas.push('nave');
    cartaActual = cartas[getRandomArbitrary()];
    max --;
    document.getElementById(cartaActual).style.display = 'inline-block';
}


function getRandomArbitrary() {
    numeroActual = Math.floor(Math.random() * max);
    return numeroActual;
}

function confirmarPalabra(){
    sndOK.play();
    alertify.alert("<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>");
    if(max == 0){
        confirmar2();
    } else {
        cartas.splice(numeroActual, 1);
        document.getElementById(cartaActual).style.display = 'none';
        document.getElementById('form'+cartaActual).style.display = 'none';
        cartaActual = cartas[getRandomArbitrary()];
        max --;
        document.getElementById(cartaActual).style.display = 'inline-block';
        document.getElementById('tablero').style.display = 'inline-block';
    }
    return false;
}

function corregirNave(){
    let letraUno = document.forms["form"+cartaActual]["letraUno"+cartaActual].value;
    let letraDos = document.forms["form"+cartaActual]["letraDos"+cartaActual].value;
    let letraTres = document.forms["form"+cartaActual]["letraTres"+cartaActual].value;
    let letraCuatro = document.forms["form"+cartaActual]["letraCuatro"+cartaActual].value;
    if(letraUno.toLowerCase() == 'n' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'v' && letraCuatro.toLowerCase() == 'e'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirLana(){
    let letraUno = document.forms["form"+cartaActual]["letraUno"+cartaActual].value;
    let letraDos = document.forms["form"+cartaActual]["letraDos"+cartaActual].value;
    let letraTres = document.forms["form"+cartaActual]["letraTres"+cartaActual].value;
    let letraCuatro = document.forms["form"+cartaActual]["letraCuatro"+cartaActual].value;
    if(letraUno.toLowerCase() == 'l' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'n' && letraCuatro.toLowerCase() == 'a'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirNena(){
    let letraUno = document.forms["form"+cartaActual]["letraUno"+cartaActual].value;
    let letraDos = document.forms["form"+cartaActual]["letraDos"+cartaActual].value;
    let letraTres = document.forms["form"+cartaActual]["letraTres"+cartaActual].value;
    let letraCuatro = document.forms["form"+cartaActual]["letraCuatro"+cartaActual].value;
    if(letraUno.toLowerCase() == 'n' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 'n' && letraCuatro.toLowerCase() == 'a'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirLoro(){
    let letraUno = document.forms["form"+cartaActual]["letraUno"+cartaActual].value;
    let letraDos = document.forms["form"+cartaActual]["letraDos"+cartaActual].value;
    let letraTres = document.forms["form"+cartaActual]["letraTres"+cartaActual].value;
    let letraCuatro = document.forms["form"+cartaActual]["letraCuatro"+cartaActual].value;
    if(letraUno.toLowerCase() == 'l' && letraDos.toLowerCase() == 'o' && letraTres.toLowerCase() == 'r' && letraCuatro.toLowerCase() == 'o'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirLuna(){
    let letraUno = document.forms["form"+cartaActual]["letraUno"+cartaActual].value;
    let letraDos = document.forms["form"+cartaActual]["letraDos"+cartaActual].value;
    let letraTres = document.forms["form"+cartaActual]["letraTres"+cartaActual].value;
    let letraCuatro = document.forms["form"+cartaActual]["letraCuatro"+cartaActual].value;
    if(letraUno.toLowerCase() == 'l' && letraDos.toLowerCase() == 'u' && letraTres.toLowerCase() == 'n' && letraCuatro.toLowerCase() == 'a'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function corregirNido(){
    let letraUno = document.forms["form"+cartaActual]["letraUno"+cartaActual].value;
    let letraDos = document.forms["form"+cartaActual]["letraDos"+cartaActual].value;
    let letraTres = document.forms["form"+cartaActual]["letraTres"+cartaActual].value;
    let letraCuatro = document.forms["form"+cartaActual]["letraCuatro"+cartaActual].value;
    if(letraUno.toLowerCase() == 'n' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'd' && letraCuatro.toLowerCase() == 'o'){
        confirmarPalabra();
    } else {
        AlertError();
    }
}

function confirmar2() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/n1j20-3.html"; //Pasa al siguiente juego
            }, 1300);
        }
    );
    return false;
}

function escribirSeguido(p){
    if(p+cartaActual=="letraUno"+cartaActual){
        document.getElementsByName("letraDos"+cartaActual)[0].focus();
    } else if(p+cartaActual=="letraDos"+cartaActual){
        document.getElementsByName("letraTres"+cartaActual)[0].focus();
    } else if(p+cartaActual=="letraTres"+cartaActual){
        document.getElementsByName("letraCuatro"+cartaActual)[0].focus();
    } else if(p+cartaActual=="letraCuatro"+cartaActual){
        document.getElementsByName("letraCinco"+cartaActual)[0].focus();
    } else if(p+cartaActual=="letraCinco"+cartaActual){
        document.getElementsByName("letraSeis"+cartaActual)[0].focus();
    }
}