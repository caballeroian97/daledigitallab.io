//nivel 1 juego 4
var sndNO = new Audio("../sonidos/error.wav");
var sndOK = new Audio("../sonidos/ganaste.wav");
let cartasNino = ["../img/niveles/aro.png", "../img/niveles/uva.png", "../img/niveles/oso.png", "../img/niveles/arbol.png", "../img/niveles/oreja.png"];
let cartasPaco = ["../img/niveles/anillo.png", "../img/niveles/unia.png", "../img/niveles/ojo.png", "../img/niveles/ola.png", "../img/niveles/ala.png"];
let sonidosNino = ["../sonidos/lentos/aro.wav", "../sonidos/lentos/uva.wav", "../sonidos/lentos/oso.wav", "../sonidos/lentos/arbol.wav", "../sonidos/lentos/oreja.wav"]
let sonidosPaco = ["../sonidos/lentos/anillo.wav", "../sonidos/lentos/unia.wav", "../sonidos/lentos/ojo.wav", "../sonidos/lentos/ola.wav", "../sonidos/lentos/ala.wav"]
var posicionNino = 0;
var puntaje = 0;
var terminoElJuego = false;

/*Nivel 1 juego 4*/

function reproducirSonido(s) {
    var sonido = new Audio("../sonidos/" + s);
    sonido.play();
}

async function sacoCartaYo() {
    const selector = document.querySelector('.imagenNino')
    if(selector.style.display == 'none'){
        verificarTerminoJuego()
        if(!terminoElJuego){
            selector.classList.add('magictime', 'tinUpIn')
            selector.style.display = 'block';
            posicionNino = Math.floor(Math.random() * cartasNino.length);
            document.getElementById('cartaNino').src=cartasNino[posicionNino];
            reproducirSonido(sonidosNino[posicionNino]);
            sacaCartaPaco();
        }
    }
}

async function sacaCartaPaco() {
    await new Promise(resolve => setTimeout(resolve, 4000));
    const selector = document.querySelector('.imagenPaco')
    selector.classList.add('magictime', 'tinUpIn')
    document.getElementById('imagenPaco').style.display = 'block';
    document.getElementById('cartaPaco').src=cartasPaco[0];
    reproducirSonido(sonidosPaco[0]);
}

async function verificarIguales(){
    let selector = document.querySelector('.imagenNino')
    if(!terminoElJuego && selector.style.display == 'block'){
        let letra1 = cartasNino[posicionNino]
        let letra2 = cartasPaco[0]
        if(letra1.slice(15,16) == letra2.slice(15,16)){
            puntaje++;
            document.getElementById("puntaje").innerHTML = puntaje;
            document.getElementById('imagenPaco').classList.remove("magictime")
            document.getElementById('imagenPaco').classList.remove("tinUpIn")
            document.getElementById('imagenNino').classList.remove("magictime")
            document.getElementById('imagenNino').classList.remove("tinUpIn")
            selector = document.querySelector('.imagenPaco')
            selector.classList.add('magictime', 'bombLeftOut')
            selector = document.querySelector('.imagenNino')
            selector.classList.add('magictime', 'bombRightOut')

            cartasNino.splice(posicionNino,1);
            sonidosNino.splice(posicionNino,1);
            cartasPaco.shift();
            sonidosPaco.shift();

            verificarTerminoJuego()
            if(!terminoElJuego){
                confirmar()
            } else {
                actividadExtra()
            }
        } else {
            incorrecto()
        }
    }
}

async function verificarDesiguales(){
    let selector = document.querySelector('.imagenNino')
    if(!terminoElJuego && selector.style.display == 'block'){
        let letra1 = cartasNino[posicionNino]
        let letra2 = cartasPaco[0]
        if(letra1.slice(15,16) != letra2.slice(15,16)){
            document.getElementById('imagenPaco').classList.remove("magictime")
            document.getElementById('imagenPaco').classList.remove("tinUpIn")
            document.getElementById('imagenNino').classList.remove("magictime")
            document.getElementById('imagenNino').classList.remove("tinUpIn")
            selector = document.querySelector('.imagenPaco')
            selector.classList.add('magictime', 'tinUpOut')
            selector = document.querySelector('.imagenNino')
            selector.classList.add('magictime', 'tinUpOut')

            var cartaNinoAlFondo = cartasNino.splice(posicionNino,1)[0];
            var sonidoNinoAlFondo = sonidosNino.splice(posicionNino,1)[0];
            var cartaPacoAlFondo = cartasPaco.shift();
            var sonidoPacoAlFondo = sonidosPaco.shift();

            cartasNino.push(cartaNinoAlFondo);
            sonidosNino.push(sonidoNinoAlFondo);
            cartasPaco.push(cartaPacoAlFondo);
            sonidosPaco.push(sonidoPacoAlFondo);

            confirmarDesigual()
        } else {
            incorrecto()
        }
    }
}

async function incorrecto(){
    document.getElementById('imagenPaco').classList.remove("magictime")
    document.getElementById('imagenPaco').classList.remove("tinUpIn")
    document.getElementById('imagenNino').classList.remove("magictime")
    document.getElementById('imagenNino').classList.remove("tinUpIn")
    let selector = document.querySelector('.imagenPaco')
    selector.classList.add('magictime', 'tinUpOut')
    selector = document.querySelector('.imagenNino')
    selector.classList.add('magictime', 'tinUpOut')

    var cartaNinoAlFondo = cartasNino.splice(posicionNino,1)[0];
    var sonidoNinoAlFondo = sonidosNino.splice(posicionNino,1)[0];
    var cartaPacoAlFondo = cartasPaco.shift();
    var sonidoPacoAlFondo = sonidosPaco.shift();

    cartasNino.push(cartaNinoAlFondo);
    sonidosNino.push(sonidoNinoAlFondo);
    cartasPaco.push(cartaPacoAlFondo);
    sonidosPaco.push(sonidoPacoAlFondo);

    confirmarIncorrecto()
}

window.onload = function(){
    document.getElementById("puntaje").innerHTML = puntaje;
};

async function confirmar() {
    sndOK.play();
    await alertify.alert("<img src='../assets/feliz.png'> <h3>&iexcl; SUMAS PUNTO, SACA OTRA CARTA !</h3>");
    await new Promise(resolve => setTimeout(resolve, 1000));
    document.getElementById('imagenPaco').classList.remove("magictime")
    document.getElementById('imagenPaco').classList.remove("bombLeftOut")
    document.getElementById('imagenNino').classList.remove("magictime")
    document.getElementById('imagenNino').classList.remove("bombRightOut")
    document.getElementById('imagenPaco').style.display = 'none';
    document.getElementById('imagenNino').style.display = 'none';
    return false;
}

async function confirmarDesigual() {
    sndOK.play();
    await alertify.alert("<img src='../assets/feliz.png'> <h3>&iexcl; SACA OTRA CARTA !</h3>");
    await new Promise(resolve => setTimeout(resolve, 1000));
    document.getElementById('imagenPaco').classList.remove("magictime")
    document.getElementById('imagenPaco').classList.remove("tinUpOut")
    document.getElementById('imagenNino').classList.remove("magictime")
    document.getElementById('imagenNino').classList.remove("tinUpOut")
    document.getElementById('imagenPaco').style.display = 'none';
    document.getElementById('imagenNino').style.display = 'none';
    return false;
}

  async function confirmarIncorrecto() {
    sndNO.play();
    await alertify.alert("<img src='../assets/triste.png'> <h3>&iexcl; TE EQUIVOCASTE, SACA OTRA CARTA !</h3>");
    await new Promise(resolve => setTimeout(resolve, 1000));
    document.getElementById('imagenPaco').classList.remove("magictime")
    document.getElementById('imagenPaco').classList.remove("tinUpOut")
    document.getElementById('imagenNino').classList.remove("magictime")
    document.getElementById('imagenNino').classList.remove("tinUpOut")
    document.getElementById('imagenPaco').style.display = 'none';
    document.getElementById('imagenNino').style.display = 'none';
    return false;
}

function verificarTerminoJuego(){
    if(cartasNino.length < 1){
        terminoElJuego = true;
    }
}

function actividadExtra(){
    sndOK.play();
    alertify.alert("<img src='../assets/feliz.png'> <h4>&iexcl; EXCELENTE ! <br>&iexcl; SIGAMOS JUGANDO ! </h4>", function(e) {
        if (e) {
            alertify.success("ELEGISTE '" + alertify.labels.ok + "'");
            setTimeout(function() {
                window.location.href = '../html/n1j4palabras.html'; //Pasa al siguiente juego
            }, 1300);
        } else {
            alertify.error("ELEGISTE '" + alertify.labels.cancel + "'");
            confirmSalida();
        }
    });
    return false
    
}

function comprobarExtra(tablaId, letras, cantidadDeLetras){

    var tabla = $('#'+tablaId);
    console.log('TABLA: ',tabla)
    var items = tabla.children('tbody').children('tr').find('img');
    console.log('ITEMS: ',items)
    var cont = 0;
    var padre;
    var hijo;

    for (var i = 0; i < items.length; i++) {
        if (items[i].dataset.letra != letras[i]){
            hijo = document.createElement("div");
            hijo.className+="item";
            hijo.appendChild(items[i]);
            padre = document.getElementById('pos-'+items[i].dataset.pos);
            padre.appendChild(hijo);
        }
        if (items[i].dataset.letra == letras[i]){
            cont++;
        }
    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
                helper: 'clone'
                
            });

            $('.box-image').droppable({
                accept: '.item',
                hoverClass: 'hovering',
                drop: function(ev, ui) {
                    ui.draggable.detach();
                    $(this).append(ui.draggable);

                }
            });

            return (cont === cantidadDeLetras)

}

function alerta() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h4> &iexcl; INTENTALO DE NUEVO !</h4>", function() {
        //aqui introducimos lo que haremos tras cerrar la alerta.
    });
}

function corregirActividadExtra(){
    let a = comprobarExtra('tablaUno',['a','n','i','ll','o'], 5)
    let b = comprobarExtra('tablaDos',['o','r','e','j','a'], 5)
    let c = comprobarExtra('tablaTres',['u','v','a'], 3)

    if (a && b && c){
        confirmarFinal();
    }
    else{
        alerta();
    }
}