var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");
var cantM = 0;
var cantO = 0;
var pintado = false
let arreglo = ["luna", "nido", "loro", "nena", "lana", "nave"];
var indexGeneral = 0;
var palabraActual = '';
var primeraVez = true;

function enmarcarMas(event) {
    selec = event.target;
    if (pintado == false) {
        selec.className += " cambiarBorde";
        pintado = true;
    } else {
        $('.cambiarBorde').removeClass("cambiarBorde");
        selec.className += " cambiarBorde";
    }

}

/*Cartelito*/

function confirmar() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
}

function confirmar2() {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
        function () {
            alertify.success("CARGANDO...");
            setTimeout(function () {
                window.location.href = "../html/n1j20-2.html"; //Pasa al siguiente juego
            }, 1300);
        }
    );
    return false;
}

function alerta() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
        function () {
            //aqui introducimos lo que haremos tras cerrar la alerta.
        }
    );
}

function faltanimg() {
    sndNO.play();
    alertify.alert("<img src='../assets/triste.png'> <h1><b> &iexcl; ALGO NO ESTA BIEN ! <br> &iexcl; INTENTALO DE NUEVO ! </b></h1>", function() {
        //aqui introducimos lo que haremos tras cerrar la alerta.
    });
}

function checkTableGameExtraJ6(){
    let tabla = $('#tabla-'+palabraActual);
    let items = tabla.children('tbody').children('tr').find('img');

    let cont = 0;
    let padre;
    let hijo;


    for (let i = 0; i < items.length; i++) {

        if (items[i].dataset.letra == palabraActual.charAt(i)){
            cont++;
        }
        if (items[i].dataset.letra != palabraActual.charAt(i)){
            alerta();
            return false;
        }

    }

    //Le devuelvo la propiedad arrastrable a cada imagen
    $('.item').draggable({
        helper: 'clone'
    });

    $('.box-image-rayitas').droppable({
        accept: '.item',
        hoverClass: 'hovering',
        drop: function(ev, ui) {
            ui.draggable.detach();
            $(this).append(ui.draggable);
        }
    });
    if(cont==4){
        document.getElementById(palabraActual).style.display = 'none';
        listadoPalabras(palabraAnterior, 'none', 'img-');
        return true;
    } else {
        return false;
    }
}

function checkGame(){
    let a = checkTableGameExtraJ6()
    if(a){
        if(arreglo.length==0){
            confirmar2()
        } else {
            confirmar();
        }
    }
}

async function generarJuego(palabra){
    document.getElementById(palabra).style.display = 'inline-block';
    listadoPalabras(palabra, 'inline-block', '')
}

function listadoPalabras(palabra, directiva, string){
    document.getElementById('tabla-'+palabra).style.display = directiva;
    switch (palabra){
        case 'nena':
        document.getElementById(string+'nena-n').style.display = directiva;
        document.getElementById(string+'nena-e').style.display = directiva;
        document.getElementById(string+'nena-n2').style.display = directiva;
        document.getElementById(string+'nena-a').style.display = directiva;
        break;

        case 'loro':
        document.getElementById(string+'loro-l').style.display = directiva;
        document.getElementById(string+'loro-o').style.display = directiva;
        document.getElementById(string+'loro-r').style.display = directiva;
        document.getElementById(string+'loro-o2').style.display = directiva;
        break;

        case 'lana':
        document.getElementById(string+'lana-l').style.display = directiva;
        document.getElementById(string+'lana-a').style.display = directiva;
        document.getElementById(string+'lana-n').style.display = directiva;
        document.getElementById(string+'lana-a2').style.display = directiva;
        break;

        case 'luna':
        document.getElementById(string+'luna-l').style.display = directiva;
        document.getElementById(string+'luna-u').style.display = directiva;
        document.getElementById(string+'luna-n').style.display = directiva;
        document.getElementById(string+'luna-a').style.display = directiva;
        break;

        case 'nave':
        document.getElementById(string+'nave-n').style.display = directiva;
        document.getElementById(string+'nave-a').style.display = directiva;
        document.getElementById(string+'nave-v').style.display = directiva;
        document.getElementById(string+'nave-e').style.display = directiva;
        break;

        case 'nido':
        document.getElementById(string+'nido-n').style.display = directiva;
        document.getElementById(string+'nido-i').style.display = directiva;
        document.getElementById(string+'nido-d').style.display = directiva;
        document.getElementById(string+'nido-o').style.display = directiva;
        break;
    }
}

function funcionGeneral(){
    if(arreglo.length>=1){
        var numeroRandom = Math.floor(Math.random() * arreglo.length);
        if(primeraVez){
            palabraAnterior = arreglo[numeroRandom];
            primeraVezVez = false;
        } else {
            palabraAnterior = palabraActual;
        }
        palabraActual = arreglo[numeroRandom];
        arreglo.splice(numeroRandom,1);
        generarJuego(palabraActual);
    }  else {
        alert('Terminó el juego')
    }
}

function checkPalabra(palabra){
    if(palabra == 'mano' || palabra == 'mapa' || palabra == 'nota'){
        if(palabra == 'mano'){
            estaBien();
            document.getElementById('mazo').style.display = 'none'
            document.getElementById('malo').style.display = 'none'
            document.getElementById('mano').style.display = 'none'
            document.getElementById('imagenMano').style.display = 'none'
            document.getElementById('maza').style.display = 'inline-block'
            document.getElementById('mapa').style.display = 'inline-block'
            document.getElementById('mata').style.display = 'inline-block'
            document.getElementById('imagenMapa').style.display = 'inline-block'
        }
        if(palabra == 'mapa'){
            estaBien();
            document.getElementById('maza').style.display = 'none'
            document.getElementById('mapa').style.display = 'none'
            document.getElementById('mata').style.display = 'none'
            document.getElementById('imagenMapa').style.display = 'none'
            document.getElementById('nata').style.display = 'inline-block'
            document.getElementById('nota').style.display = 'inline-block'
            document.getElementById('neta').style.display = 'inline-block'
            document.getElementById('imagenNota').style.display = 'inline-block'
        }
        if(palabra == 'nota'){
            confirmarFinal();
        }
    } else {
        alerta()
    }
}

function estaBien() {
    sndOK.play();
    alertify.alert( "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>");
}