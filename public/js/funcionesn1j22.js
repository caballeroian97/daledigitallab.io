var ficha= false;
var fumar = false;
var seis= false;
var saco = false;
var semilla= false;

var arreglo = [];
var arreglo2 = ["ficha", "fumar", "seis", "saco", "semilla"]

async function cargarCartas(){
    arreglo["ficha"] = false;
    arreglo["fumar"] = false;
    arreglo["seis"] = false;
    arreglo["saco"] = false;
    arreglo["semilla"] = false;
    
    for(i=0;i<5;i++){
        const selector = document.getElementById(arreglo2[i]);
        selector.classList.add('magictime', 'tinUpIn');
        document.getElementById(arreglo2[i]).style.display = 'inline-block';
        document.getElementById("form22"+arreglo2[i]).style.display = 'none';
        selector.classList.toggle('hover');
        playSound('../sonidos/'+arreglo2[i]+'.wav')
        await new Promise(resolve => setTimeout(resolve, 5000));
    }

    setTimeout(function () {
      document.getElementById("fichaFija").style.display = 'none';
      document.getElementById("fichaTapada").style.display = 'block';

      document.getElementById("fumarFija").style.display = 'none';
      document.getElementById("fumarTapada").style.display = 'block';

      document.getElementById("seisFija").style.display = 'none';
      document.getElementById("seisTapada").style.display = 'block';

      document.getElementById("sacoFija").style.display = 'none';
      document.getElementById("sacoTapada").style.display = 'block';

      document.getElementById("semillaFija").style.display = 'none';
      document.getElementById("semillaTapada").style.display = 'block';
    }, 0);
    
    for(i=0;i<5;i++){
      document.getElementById("form22"+arreglo2[i]).style.display = 'inline-block';
    }
}

function comenzar(){
    document.getElementById("cartelComenzar").style.display = 'none';
    document.getElementById("juego").style.display = 'block';
    cargarCartas()
};

function enmarcarVarias(event) {
    var carta = event.target;
    if (arreglo[carta.id] == false) {
        carta.className += " cambiarBorde";
        arreglo[carta.id] = true;
    } else {
        document.getElementById(carta.id).classList.remove("cambiarBorde");
        arreglo[carta.id] = false;
    }
}

function escribirSeguidoFicha(p){
  if(p=="letraUnoFicha"){
      document.getElementById("letraDosFicha").focus();
  } else if(p=="letraDosFicha"){
      document.getElementById("letraTresFicha").focus();
  } else if(p=="letraTresFicha" && document.getElementById(p).value.length > 1){
      document.getElementById("letraCuatroFicha").focus();
  }
}

function escribirSeguidoFumar(p){
  if(p=="letraUnoFumar"){
      document.getElementById("letraDosFumar").focus();
  } else if(p=="letraDosFumar"){
      document.getElementById("letraTresFumar").focus();
  } else if(p=="letraTresFumar"){
      document.getElementById("letraCuatroFumar").focus();
  } else if(p=="letraCuatroFumar"){
      document.getElementById("letraCincoFumar").focus();
  }
}

function escribirSeguidoSeis(p){
  if(p=="letraUnoSeis"){
      document.getElementById("letraDosSeis").focus();
  } else if(p=="letraDosSeis"){
      document.getElementById("letraTresSeis").focus();
  } else if(p=="letraTresSeis"){
      document.getElementById("letraCuatroSeis").focus();
  }
}

function escribirSeguidoSaco(p){
  if(p=="letraUnoSaco"){
      document.getElementById("letraDosSaco").focus();
  } else if(p=="letraDosSaco"){
      document.getElementById("letraTresSaco").focus();
  } else if(p=="letraTresSaco"){
      document.getElementById("letraCuatroSaco").focus();
  }
}

function escribirSeguidoSemilla(p){
  if(p=="letraUnoSemilla"){
      document.getElementById("letraDosSemilla").focus();
  } else if(p=="letraDosSemilla"){
      document.getElementById("letraTresSemilla").focus();
  } else if(p=="letraTresSemilla"){
      document.getElementById("letraCuatroSemilla").focus();
  } else if(p=="letraCuatroSemilla"){
      document.getElementById("letraCincoSemilla").focus();
  } else if(p=="letraCincoSemilla" && document.getElementById(p).value.length > 1){
    document.getElementById("letraSeisSemilla").focus();
  }
}

function corregirFicha(){
  let letraUno = document.forms["form22ficha"]["letraUnoFicha"].value;
  let letraDos = document.forms["form22ficha"]["letraDosFicha"].value;
  let letraTres = document.forms["form22ficha"]["letraTresFicha"].value;
  let letraCuatro = document.forms["form22ficha"]["letraCuatroFicha"].value;
  if(letraUno.toLowerCase() == 'f' && letraDos.toLowerCase() == 'i' && letraTres.toLowerCase() == 'ch' && letraCuatro.toLowerCase() == 'a'){
    ficha = true;
  } 
}

function corregirFumar(){
  let letraUno = document.forms["form22fumar"]["letraUnoFumar"].value;
  let letraDos = document.forms["form22fumar"]["letraDosFumar"].value;
  let letraTres = document.forms["form22fumar"]["letraTresFumar"].value;
  let letraCuatro = document.forms["form22fumar"]["letraCuatroFumar"].value;
  let letraCinco = document.forms["form22fumar"]["letraCincoFumar"].value;
  if(letraUno.toLowerCase() == 'f' && letraDos.toLowerCase() == 'u' && letraTres.toLowerCase() == 'm' && letraCuatro.toLowerCase() == 'a' && letraCinco.toLowerCase() == 'r'){
    fumar = true;
  } 
}

function corregirSeis(){
  let letraUno = document.forms["form22seis"]["letraUnoSeis"].value;
  let letraDos = document.forms["form22seis"]["letraDosSeis"].value;
  let letraTres = document.forms["form22seis"]["letraTresSeis"].value;
  let letraCuatro = document.forms["form22seis"]["letraCuatroSeis"].value;
  if(letraUno.toLowerCase() == 's' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 'i' && letraCuatro.toLowerCase() == 's'){
    seis = true;
  } 
}

function corregirSaco(){
  let letraUno = document.forms["form22saco"]["letraUnoSaco"].value;
  let letraDos = document.forms["form22saco"]["letraDosSaco"].value;
  let letraTres = document.forms["form22saco"]["letraTresSaco"].value;
  let letraCuatro = document.forms["form22saco"]["letraCuatroSaco"].value;
  if(letraUno.toLowerCase() == 's' && letraDos.toLowerCase() == 'a' && letraTres.toLowerCase() == 'c' && letraCuatro.toLowerCase() == 'o'){
    saco = true;
  }
}

function corregirSemilla(){
  let letraUno = document.forms["form22semilla"]["letraUnoSemilla"].value;
  let letraDos = document.forms["form22semilla"]["letraDosSemilla"].value;
  let letraTres = document.forms["form22semilla"]["letraTresSemilla"].value;
  let letraCuatro = document.forms["form22semilla"]["letraCuatroSemilla"].value;
  let letraCinco = document.forms["form22semilla"]["letraCincoSemilla"].value;
  let letraSeis = document.forms["form22semilla"]["letraSeisSemilla"].value;
  if(letraUno.toLowerCase() == 's' && letraDos.toLowerCase() == 'e' && letraTres.toLowerCase() == 'm' && letraCuatro.toLowerCase() == 'i' && letraCinco.toLowerCase() == 'll' && letraSeis.toLowerCase() == 'a'){
    semilla = true;
  } 
}

function corregir(p){
  corregirFicha();
  corregirFumar();
  corregirSeis();
  corregirSaco();
  corregirSemilla();

  if(ficha && fumar && seis && saco && semilla){
    confirmarFinal();
  } else {
    alerta();
  }
}

function alerta() {
  sndNO.play();
  alertify.alert(
      "<img src='../assets/triste.png'> <h3> &iexcl; FALTAN CARTAS ! </b></h3>",
      function () {
          //aqui introducimos lo que haremos tras cerrar la alerta.
      }
  );
}

const quitarAnimacion = () => {
    let element = document.getElementById("ayudabtn");
    element.classList.remove("pulse");
    let ocultarMano = document.getElementById("manoayuda")
    ocultarMano.classList.add("imgOcultar")
}

const ocultarElement = id => {
  let elem = document.getElementById(id)
  elem.classList.add("imgOcultar")
}

const quitarAnim = id => {
  let elem = document.getElementById(id)
  elem.classList.remove("pulse")
}

const mostrarElement = id => {
  let elem = document.getElementById(id)
  elem.classList.remove("imgOcultar")
}

const animar = (id) => {
  let elem = document.getElementById(id)
  elem.classList.add("pulse")
}

const mostrarImgPrincipal = (url,titulo) => {
 
  let tabla = document.getElementById("tabla1-1")
  tabla.classList.remove("imgOcultar")
  tabla.classList.add("tabla1-1")

  let elementImg = document.getElementById("imgj1");
  elementImg.classList.add("imgJuegosG")
  console.log('elemimg',elementImg)
  let elementTitulo = document.getElementById("tituloj1");
  elementImg.src=url;
  elementTitulo.innerHTML= titulo
}

/*Selecciona con color una imagen elegida, y comprueba que 
no haya otra seleccionada, en ese caso, la despinta, y pinta la nueva */
function enmarcar(event) {

  animar('j1-btncheck')
  mostrarElement('manoayuda3')

  let element = document.getElementById("manoayuda2")
  element.classList.add("imgOcultar")
 
  let selec = event.target;
  if (pintado == false) {
    selec.className += "cambiarBorde";
    pintado = true;
    letraSelec = selec.id;
   
  } else {
    $(".cambiarBorde").removeClass("cambiarBorde");
    selec.className += " cambiarBorde";
    letraSelec = selec.id;
   
  }
}
var sndOK = new Audio("../sonidos/ganaste.wav");
var sndNO = new Audio("../sonidos/error.wav");

/*Cartelito*/

function confirmar(s) {
  sndOK.play();
  alertify.alert(
    "<img src='../assets/feliz.png'> <h3>&iexcl; GANASTE !</h3>",
    function () {
        window.location.href = "../html/" + s + ".html";
    }
  );
  return false;
}

function alerta() {
  //un alert
  sndNO.play();
  alertify.alert(
    "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
    function () {
      //aqui introducimos lo que haremos tras cerrar la alerta.
    }
  );
}

