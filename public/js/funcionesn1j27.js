var arreglo = ["gasa", "gotera", "gallina", "cocina", "corona", "cura"]
var correctas = 0;
var cartasRestantes = 6
var cartaActual = null

async function autoPlay(){
    await new Promise(resolve => setTimeout(resolve, 8000));
    document.getElementById('juego').style.display = 'inline-block';
    cargarCartas();
}

async function cargarCartas(){
    for(i=0;i<6;i++){
        const selector = document.getElementById(arreglo[i]);
        selector.classList.add('magictime', 'tinUpIn');
        document.getElementById(arreglo[i]).style.display = 'inline-block';
        playSound('../sonidos/'+arreglo[i]+'.wav')
        await new Promise(resolve => setTimeout(resolve, 5000));
    }
}

function cargarDefinicion(){
    cartaActual = arreglo[getRandomArbitrary()];
    playSound('../sonidos/'+cartaActual+'Definicion.wav')
}

function getRandomArbitrary() {
    numeroActual = Math.floor(Math.random() * cartasRestantes);
    return numeroActual;
}

function corregir(cartaSeleccionada){
    if(cartaActual != null){
        if(cartaSeleccionada === cartaActual){
            correctas++
            cartasRestantes--
            document.getElementById(cartaActual).setAttribute('style', 'display:none !important');
            arreglo.splice(arreglo.indexOf(cartaActual), 1)
            if(correctas == 6){
                confirmarFinal();
            } else {
                correcto();
            }
        } else {
            incorrecto();
        }
    } else {
        errorEscuchar();
    }
}

function correcto(s) {
    sndOK.play();
    alertify.alert(
        "<img src='../assets/feliz.png'> <h3>&iexcl; EXCELENTE !</h3>",
    );
    return false;
}

function errorEscuchar() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; ESCUCHA LA DEFINICION ! </b></h3>",
    );
}

function incorrecto() {
    sndNO.play();
    alertify.alert(
        "<img src='../assets/triste.png'> <h3> &iexcl; INTENTALO DE NUEVO ! </b></h3>",
    );
}

